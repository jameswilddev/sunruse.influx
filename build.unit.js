describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("build", function(){
            var instance, stepStart, stepEnd, success, failure
            beforeEach(function(){
                spyOn(Date, "now").and.returnValue(300)
                instance = SUNRUSE.influx()
                instance.buildSteps = [{
                    name: "stepA",
                    perform: jasmine.createSpy()
                }, {
                    name: "stepB",
                    perform: jasmine.createSpy()
                }, {
                    name: "stepC",
                    perform: jasmine.createSpy()
                }]
                stepStart = jasmine.createSpy()
                stepEnd = jasmine.createSpy()
                success = jasmine.createSpy()
                failure = jasmine.createSpy()
            })
            
            it("executes every perform function in order passing through results", function(){
                instance.buildSteps[0].perform.and.callFake(function(input){
                    expect(input).toEqual("Test Input A")
                    expect(instance.buildSteps[1].perform).not.toHaveBeenCalled()
                    return "Test Input B"
                })
                
                instance.buildSteps[1].perform.and.callFake(function(input){
                    expect(input).toEqual("Test Input B")
                    expect(instance.buildSteps[2].perform).not.toHaveBeenCalled()
                    return "Test Input C"
                })                
                
                instance.buildSteps[2].perform.and.callFake(function(input){
                    expect(input).toEqual("Test Input C")
                })
                
                instance.build("Test Input A", stepStart, stepEnd, success, failure)
                
                expect(instance.buildSteps[0].perform.calls.count()).toEqual(1)
                expect(instance.buildSteps[1].perform.calls.count()).toEqual(1)
                expect(instance.buildSteps[2].perform.calls.count()).toEqual(1)
            })
            
            it("executes the starting stage callback for each stage before the perform function is called", function(){
                instance.buildSteps[0].perform.and.callFake(function(input){
                    expect(stepStart.calls.count()).toEqual(1)
                    expect(stepStart).toHaveBeenCalledWith("stepA", 0, 3)
                })
                
                instance.buildSteps[1].perform.and.callFake(function(input){
                    expect(stepStart.calls.count()).toEqual(2)
                    expect(stepStart).toHaveBeenCalledWith("stepB", 1, 3)
                })                
                
                instance.buildSteps[2].perform.and.callFake(function(input){
                    expect(stepStart.calls.count()).toEqual(3)
                    expect(stepStart).toHaveBeenCalledWith("stepC", 2, 3)
                })                
                
                instance.build(null, stepStart, stepEnd, success, failure)
                
                expect(stepStart.calls.count()).toEqual(3)
            })
            
            it("executes the completed stage callback for each stage after the perform function is called", function(){
                var callId = 0
                stepEnd.and.callFake(function(name, completed, total){
                    callId++
                    expect(callId).not.toEqual(4)
                    expect(stepStart.calls.count()).toEqual(callId)
                    expect(completed).toEqual(callId)
                    expect(total).toEqual(3)
                    
                    switch(callId) {
                        case 1:
                            expect(name).toEqual("stepA")
                            expect(instance.buildSteps[0].perform).toHaveBeenCalled()
                            break
                            
                        case 2:
                            expect(name).toEqual("stepB")
                            expect(instance.buildSteps[1].perform).toHaveBeenCalled()
                            break
                            
                        case 3:
                            expect(name).toEqual("stepC")
                            expect(instance.buildSteps[2].perform).toHaveBeenCalled()
                            break                            
                    }
                })
                
                instance.build(null, stepStart, stepEnd, success, failure)
                
                expect(stepEnd.calls.count()).toEqual(3)
            })
            
            it("stops on encountering an expected exception and executes the failure callback", function(){
                instance.buildSteps[1].perform.and.callFake(function(){
                    Date.now.and.returnValue(540)
                    throw {
                        line: "Test Line",
                        message: "Test Message"
                    }
                })
                
                instance.build(null, stepStart, stepEnd, success, failure)
                
                expect(instance.buildSteps[2].perform).not.toHaveBeenCalled()
                
                expect(stepStart.calls.count()).toEqual(2)
                expect(stepEnd.calls.count()).toEqual(1)
                expect(success).not.toHaveBeenCalled()
                expect(failure.calls.count()).toEqual(1)
                expect(failure).toHaveBeenCalledWith("Test Line", "Test Message", 240, 1, 3)
            })
            
            it("bubbles up unexpected exceptions", function(){
                instance.buildSteps[1].perform.and.callFake(function(){
                    Date.now.and.returnValue(540)
                    throw "Unexpected Exception"
                })
                
                expect(function(){
                    instance.build(null, stepStart, stepEnd, success, failure)
                }).toThrow("Unexpected Exception")
                
                expect(instance.buildSteps[2].perform).not.toHaveBeenCalled()
                
                expect(stepStart.calls.count()).toEqual(2)
                expect(stepEnd.calls.count()).toEqual(1)
                expect(success).not.toHaveBeenCalled()
                expect(failure).not.toHaveBeenCalled()
            })
            
            it("executes the success callback with the last stage's result and the build time on completing successfully", function(){
                var callId = 0
                stepEnd.and.callFake(function(){
                    expect(success).not.toHaveBeenCalled()
                    if(callId++ == 2) Date.now.and.returnValue(540)
                })
                
                instance.buildSteps[2].perform.and.returnValue("Test Output")
                
                instance.build(null, stepStart, stepEnd, success, failure)
                
                expect(stepStart.calls.count()).toEqual(3)
                expect(stepEnd.calls.count()).toEqual(3)
                expect(failure).not.toHaveBeenCalled()
                expect(success.calls.count()).toEqual(1)
                expect(success).toHaveBeenCalledWith("Test Output", 240)
            })
        })
    })
})