describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("typecheckFieldChains", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "typecheckChain")
                spyOn(instance, "getFieldsByTypeReference").and.callFake(function(typeReference, parsedTypes){
                    expect(typeReference).toEqual("test return type")
                    expect(parsedTypes).toEqual("test parsed types")
                    return ["test type a", "test type b", "test type c", "test type d"]
                })
            })
            
            it("throws an exception when fewer fields were specified than exist in the type", function(){
                expect(function(){
                    instance.typecheckFieldChains({
                        returns: "test return type",
                        fields: ["test field a", "test field b", "test field c"],
                        line: "test line"
                    }, "test function", "test parsed types")                    
                }).toThrow({
                    message: "incorrectNumberOfFieldsReturned",
                    line: "test line"
                })
            })
            
            it("throws an exception when more fields were specified than exist in the type", function(){
                expect(function(){
                    instance.typecheckFieldChains({
                        returns: "test return type",
                        fields: ["test field a", "test field b", "test field c", "test field d", "test field e"],
                        line: "test line"
                    }, "test function", "test parsed types")                    
                }).toThrow({
                    message: "incorrectNumberOfFieldsReturned",
                    line: "test line"
                })
            })
            
            it("typechecks each field", function(){
                instance.typecheckFieldChains({
                    returns: "test return type",
                    fields: ["test field a", "test field b", "test field c", "test field d"],
                    line: "test line"
                }, "test function", "test parsed types")
                
                expect(instance.typecheckChain).toHaveBeenCalledWith("test field a", "test function", "test type a", "test parsed types")
                expect(instance.typecheckChain).toHaveBeenCalledWith("test field b", "test function", "test type b", "test parsed types")
                expect(instance.typecheckChain).toHaveBeenCalledWith("test field c", "test function", "test type c", "test parsed types")
                expect(instance.typecheckChain).toHaveBeenCalledWith("test field d", "test function", "test type d", "test parsed types")
            })
        })
    })
})