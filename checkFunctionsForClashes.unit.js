describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("checkFunctionsForClashes", function(){
            var instance, types, run, functionMatch, functionNonGenericFieldMatch, functionGenericFieldMatch, nativeFunctionMatch, nativeFunctionNonGenericFieldMatch, nativeFunctionGenericFieldMatch, nativeFunctionFunctionMatch
            beforeEach(function(){
                instance = SUNRUSE.influx()
                instance.nativeFunctions = [{
                    name: "nativeFunctionA",
                    line: "Test Line Native",
                    takes: "Test Native Function A Takes"
                }, {
                    name: "nativeFunctionB",
                    line: "Test Line Native",
                    takes: "Test Native Function B Takes"
                }, {
                    name: "nativeFunctionC",
                    line: "Test Other Native Line A",
                    takes: "Test Native Function C Takes"
                }]
                types = {
                    aliases: {},
                    genericAliases: {},
                    combinations: {
                        nonGenericCombinationA: {
                            fields: {
                                fieldAA: {}
                            }
                        }
                    },
                    genericCombinations: {
                        genericCombinationB: {
                            fields: {
                                fieldBA: {}
                            }
                        }
                    },
                    functions: [{
                        name: "functionA",
                        line: "Test Line",
                        takes: "Test Function A Takes"
                    }, {
                        name: "functionB",
                        line: "Test Line",
                        takes: "Test Function B Takes"
                    }, {
                        name: "functionC",
                        line: "Test Other Line A",
                        takes: "Test Function C Takes"
                    }]
                }
                spyOn(instance, "typeReferencesMatch").and.callFake(function(a, b){
                    var pair = [a, b]
                    if(pair.indexOf("Test Native Function A Takes") != -1) {
                        if(pair.indexOf("Test Native Function B Takes") != -1) return nativeFunctionMatch
                        if(pair.indexOf("Test Function A Takes") != -1) return nativeFunctionFunctionMatch
                        var other = pair[1 - pair.indexOf("Test Native Function A Takes")]
                        if(other.length == 1 && other[0] == "nonGenericCombinationA") return nativeFunctionNonGenericFieldMatch
                        if(other.length == 2 && other[0] == "?" && other[1] == "genericCombinationB") return nativeFunctionGenericFieldMatch
                        return
                    }
                    if(pair.indexOf("Test Function A Takes") == -1) return false
                    if(pair.indexOf("Test Function B Takes") != -1) return functionMatch
                    var other = pair[1 - pair.indexOf("Test Function A Takes")]
                    if(other.length == 1 && other[0] == "nonGenericCombinationA") return functionNonGenericFieldMatch
                    if(other.length == 2 && other[0] == "?" && other[1] == "genericCombinationB") return functionGenericFieldMatch
                })
                run = function(){
                    instance.checkFunctionsForClashes(types)
                }
            })

            describe("function-function", function(){
                it("does not throw an exception when functions with distinct names exist against the same type", function(){
                    functionMatch = true
                    run()
                })
                
                it("does not throw an exception when functions with the same names exist against different types", function(){
                    functionMatch = false
                    types.functions[1].name = "functionA"
                    run()
                })            
                
                it("throws an exception when functions with the same names exist against the same type", function(){
                    functionMatch = true
                    types.functions[1].name = "functionA"
                    expect(run).toThrow({
                        message: "functionNamesNotUniqueToInputType",
                        line: "Test Line"
                    })
                })       
            })
            
            describe("function-nongeneric field", function(){
                it("does not throw an exception when functions take a nongeneric combination as an input without field name-function name clashes", function(){
                    functionNonGenericFieldMatch = true
                    run()
                })
            
                it("does not throw an exception when functions exist with the same name as fields in nongeneric combination types they do not apply to", function(){
                    functionNonGenericFieldMatch = false
                    types.combinations.nonGenericCombinationA.fields.functionA = types.combinations.nonGenericCombinationA.fields.fieldAA
                    delete types.combinations.nonGenericCombinationA.fields.fieldAA
                    run()
                })            
                
                it("throws an exception when functions apply to nongeneric combination types with fields of the same name", function(){
                    functionNonGenericFieldMatch = true
                    types.combinations.nonGenericCombinationA.fields.functionA = types.combinations.nonGenericCombinationA.fields.fieldAA
                    delete types.combinations.nonGenericCombinationA.fields.fieldAA
                    expect(run).toThrow({
                        message: "fieldAndFunctionNameShareNameAndApplicableTypes",
                        line: "Test Line"
                    })
                })                                        
            })
            
            describe("function-generic field", function(){
                it("does not throw an exception when functions take a generic combination as an input without field name-function name clashes", function(){
                    functionGenericFieldMatch = true
                    run()
                })
            
                it("does not throw an exception when functions exist with the same name as fields in generic combination types they do not apply to", function(){
                    functionGenericFieldMatch = false
                    types.genericCombinations.genericCombinationB.fields.functionA = types.genericCombinations.genericCombinationB.fields.fieldBA
                    delete types.genericCombinations.genericCombinationB.fields.fieldBA
                    run()
                })            
                
                it("throws an exception when functions apply to generic combination types with fields of the same name", function(){
                    functionGenericFieldMatch = true
                    types.genericCombinations.genericCombinationB.fields.functionA = types.genericCombinations.genericCombinationB.fields.fieldBA
                    delete types.genericCombinations.genericCombinationB.fields.fieldBA
                    expect(run).toThrow({
                        message: "fieldAndFunctionNameShareNameAndApplicableTypes",
                        line: "Test Line"
                    })
                })                
            })
            
            describe("native function-function", function(){
                it("does not throw an exception when functions with distinct names exist against the same type", function(){
                    nativeFunctionFunctionMatch = true
                    run()
                })
                
                it("does not throw an exception when functions with the same names exist against different types", function(){
                    nativeFunctionFunctionMatch = false
                    instance.nativeFunctions[0].name = "functionA"
                    run()
                })            
                
                it("throws an exception when functions with the same names exist against the same type", function(){
                    nativeFunctionFunctionMatch = true
                    instance.nativeFunctions[0].name = "functionA"
                    instance.nativeFunctions[0].line = "Test Line Alternate"
                    types.functions[0].line = "Test Line Alternate"
                    expect(run).toThrow({
                        message: "functionNamesNotUniqueToInputType",
                        line: "Test Line Alternate"
                    })
                })       
            })
            
            describe("native function-native function", function(){
                it("does not throw an exception when functions with distinct names exist against the same type", function(){
                    nativeFunctionMatch = true
                    run()
                })
                
                it("does not throw an exception when functions with the same names exist against different types", function(){
                    nativeFunctionMatch = false
                    instance.nativeFunctions[1].name = "nativeFunctionA"
                    run()
                })            
                
                it("throws an exception when functions with the same names exist against the same type", function(){
                    nativeFunctionMatch = true
                    instance.nativeFunctions[1].name = "nativeFunctionA"
                    expect(run).toThrow({
                        message: "functionNamesNotUniqueToInputType",
                        line: "Test Line Native"
                    })
                })       
            })
            
            describe("native function-nongeneric field", function(){
                it("does not throw an exception when functions take a nongeneric combination as an input without field name-function name clashes", function(){
                    nativeFunctionNonGenericFieldMatch = true
                    run()
                })
            
                it("does not throw an exception when functions exist with the same name as fields in nongeneric combination types they do not apply to", function(){
                    nativeFunctionNonGenericFieldMatch = false
                    types.combinations.nonGenericCombinationA.fields.nativeFunctionA = types.combinations.nonGenericCombinationA.fields.fieldAA
                    delete types.combinations.nonGenericCombinationA.fields.fieldAA
                    run()
                })            
                
                it("throws an exception when functions apply to nongeneric combination types with fields of the same name", function(){
                    nativeFunctionNonGenericFieldMatch = true
                    types.combinations.nonGenericCombinationA.fields.nativeFunctionA = types.combinations.nonGenericCombinationA.fields.fieldAA
                    delete types.combinations.nonGenericCombinationA.fields.fieldAA
                    expect(run).toThrow({
                        message: "fieldAndFunctionNameShareNameAndApplicableTypes",
                        line: "Test Line Native"
                    })
                })                                        
            })
            
            describe("native function-generic field", function(){
                it("does not throw an exception when functions take a generic combination as an input without field name-function name clashes", function(){
                    nativeFunctionGenericFieldMatch = true
                    run()
                })
            
                it("does not throw an exception when functions exist with the same name as fields in generic combination types they do not apply to", function(){
                    nativeFunctionGenericFieldMatch = false
                    types.genericCombinations.genericCombinationB.fields.nativeFunctionA = types.genericCombinations.genericCombinationB.fields.fieldBA
                    delete types.genericCombinations.genericCombinationB.fields.fieldBA
                    run()
                })            
                
                it("throws an exception when functions apply to generic combination types with fields of the same name", function(){
                    nativeFunctionGenericFieldMatch = true
                    types.genericCombinations.genericCombinationB.fields.nativeFunctionA = types.genericCombinations.genericCombinationB.fields.fieldBA
                    delete types.genericCombinations.genericCombinationB.fields.fieldBA
                    expect(run).toThrow({
                        message: "fieldAndFunctionNameShareNameAndApplicableTypes",
                        line: "Test Line Native"
                    })
                })                
            })            
        })
    })
})