SUNRUSE = {
    influx: function(){
        var instance = {
            // An object defining the supported primitives, where the keys are
            // the type names and the values are objects containing:
            //  regex: A regular expression which matches literals of the 
            //         primitive.
            primitives: {},
            
            // Functions implemented by the compiler.  These are the same as
            // functions returned by parse, except that rather than defining
            // fields, a "generateCode" function is defined.  This takes:
            //   input: The data being given.  Combinations are objects
            //          containing properties for each field keyed by name,
            //          while primitives are objects containing:
            //        constant: The primitive literal as a string when truthy.
            //        reference: A compiler-defined identifier for the value
            //                   which will only be known at runtime.
            //   id: Returns an integer unique to this run of the compiler on
            //       each call.
            //   generic: The generic parameter for the function, if any.
            //   lines: The generated code so far.  Add strings to this to add
            //          new lines of code.
            // Return in the same format as input.
            nativeFunctions: [],

            // Given what may be a primitive literal, returns the name of the
            // matching primitive type, or falsy if none could be found.
            getPrimitiveTypeNameByLiteral: function(literal) {
                for(var name in instance.primitives)
                    if(literal.match(instance.primitives[name].regex)) return name
            },
            
            // Given an array of tokens, ensures that it could be a type 
            // reference syntactically; ? and primitive names are only allowed 
            // at the bottom of the chain.  Primitive literals are not allowed
            // anywhere in the chain.  The second argument is the source line,
            // used for error throwing.  ? is only allowed at all if the third
            // argument is truthy.  The keywords "let", "combines", "takes" and
            // "returns" are not allowed.
            validateTypeReferenceSyntax: function(tokens, line, genericsAllowed) {
                if(!tokens.length) throw {
                    line: line,
                    message: "typeReferenceEmpty"
                }

                if(tokens[0] == "?" && !genericsAllowed) throw {
                    line: line,
                    message: "genericTypeUsedInNonGenericContext"
                }
                
                for(var tokenId = 1; tokenId < tokens.length; tokenId++) {
                    if(tokens[tokenId] == "?") throw {
                        line: line,
                        message: "onlyTheBaseTypeMayBeGeneric"
                    }
                    
                    if(instance.primitives[tokens[tokenId]]) throw {
                        line: line,
                        message: "primitiveTypesMayNotBeUsedAsGenericCombinations"
                    }
                }
                
                for(var tokenId = 0; tokenId < tokens.length; tokenId++) {
                    if(["let", "takes", "returns", "combines", "input"].indexOf(tokens[tokenId]) != -1) throw {
                        line: line,
                        message: "typeReferencesMayNotContainKeywords"
                    }
                    
                    if(instance.getPrimitiveTypeNameByLiteral(tokens[tokenId]))
                        throw {
                            line: line,
                            message: "typeReferencesMayNotContainPrimitiveLiterals"
                        }
                }
            },
            
            // Given an array of tokens, generates a chain object as described
            // by parse.  The second argument is the source line, used for error
            // throwing.
            validateAndCreateChain: function(tokens, line) {
                if(!tokens.length) throw {
                    line: line,
                    message: "chainEmpty"
                }
                
                var output = {
                    line: line,
                    functions: tokens.slice(1)
                }
                
                switch(tokens[0]) {
                    case "let":
                    case "takes":
                    case "returns":
                    case "combines":
                        throw {
                            message: "valueExpectedKeywordGiven",
                            line: line
                        }
                        
                    case "input":
                        output.input = true
                        break
                        
                    default:
                        if(instance.getPrimitiveTypeNameByLiteral(tokens[0]))
                            output.primitive = tokens[0]
                        else
                            output.let = tokens[0]
                        break
                }
                
                for(var i = 0; i < output.functions.length; i++) {
                    var token = output.functions[i]
                    switch(token) {
                        case "input":
                        case "let":
                        case "returns":
                        case "takes":
                        case "combines":
                            throw {
                                line: line,
                                message: "functionExpectedKeywordGiven"
                            }
                            
                        default:
                            if(instance.getPrimitiveTypeNameByLiteral(token)) throw {
                                line: line,
                                message: "functionExpectedPrimitiveLiteralGiven"
                            }
                            
                            if(instance.primitives[token]) throw {
                                line: line,
                                message: "functionExpectedPrimitiveGiven"
                            }
                            break
                    }
                }
                
                return output
            },
            
            // Given a let or function, that function or the let's parent 
            // function and the parsed types, typechecks every field chain in 
            // the let/function.
            typecheckFieldChains: function(letOrFunction, funct, parsed) {
                var fields = instance.getFieldsByTypeReference(letOrFunction.returns, parsed)
                if(letOrFunction.fields.length != fields.length) throw {
                    message: "incorrectNumberOfFieldsReturned",
                    line: letOrFunction.line
                }
                for(var fieldId = 0; fieldId < fields.length; fieldId++)
                    instance.typecheckChain(letOrFunction.fields[fieldId], funct, fields[fieldId], parsed)
            },
            
            // Given a chain, either from a field or a let, the function from
            // which it originated, the type reference it is expected to return
            // and the parsed types, determines the input type and calls through 
            // to typecheckChainFunctions to ensure the chain fulfils the output
            // type
            typecheckChain: function(chain, funct, outputs, parsed) {
                if(chain.let) {
                    if(!funct.lets[chain.let]) throw {
                        message: "inputUnresolvable",
                        line: chain.line
                    }
                    instance.typecheckChainFunctions(funct.lets[chain.let].returns, chain.functions, outputs, chain.line, parsed)
                } else if(chain.input)
                    instance.typecheckChainFunctions(funct.takes, chain.functions, outputs, chain.line, parsed)
                else
                    instance.typecheckChainFunctions([instance.getPrimitiveTypeNameByLiteral(chain.primitive)], chain.functions, outputs, chain.line, parsed)
            },
            
            // Given the generic context, if any, an input type reference, an 
            // array of the function names in the chain, the expected output 
            // type, the line on which the chain was declared and the parsed 
            // types, validates that:
            //  - Every function in the chain is resolvable.
            //  - The type received at the end is as expected.
            typecheckChainFunctions: function(inputType, functionNames, outputType, line, parsed){
                for(var tokenId = 0; tokenId < functionNames.length; tokenId++) {
                    var field = instance.findFieldByInputAndName(inputType, functionNames[tokenId], parsed)
                    if(field) {
                        inputType = field.of
                        continue
                    }
                    var funct = instance.findFunctionByInputAndName(inputType, functionNames[tokenId], parsed)
                    if(!funct) throw {
                        message: "functionInChainUnresolvable",
                        name: functionNames[tokenId],
                        taking: inputType,
                        line: line
                    }
                    inputType = funct.returns
                }
                
                if(inputType.length == outputType.length) {
                    var i
                    for(i = 0; i < inputType.length; i++)
                        if(inputType[i] != outputType[i]) break
                    if(i == inputType.length) return
                }
                throw {
                    message: "typeAtEndOfChainIncorrect",
                    expected: outputType,
                    actual: inputType,
                    line: line
                }
            },
            
            // Given a parsed function and the parsed types it is from, ensures 
            // that its lets contain no circular or unresolvable references and
            // typechecks every chain.
            checkFunctionLets: function(funct, parsed) {
                var resolvedLets = {}
                var remainingLets = {}
                for(var letId in funct.lets) {
                    var lett = funct.lets[letId]
                    instance.typecheckFieldChains(lett, funct, parsed)
                    if(lett.let && funct.lets[lett.let]) {
                        remainingLets[letId] = lett
                    } else
                        resolvedLets[letId] = lett
                }

                var resolved = true
                while(resolved) {
                    resolved = false
                    for(var letId in remainingLets) {
                        var lett = remainingLets[letId]
                        if(resolvedLets[lett.let]) {
                            resolvedLets[letId] = lett
                            delete remainingLets[letId]
                            resolved = true
                        }
                    }
                }

                for(var letId in remainingLets) throw {
                    message: "circularDependencyInLets",
                    line: remainingLets[letId].line
                }
            },

            // Given an array of tokens, ensures that it could be a name 
            // syntactically; primitive names and literals are disallowed, as
            // are keywords, and only the first may be a ?.  The second argument 
            // is the source line, used for error throwing.  ? is only allowed 
            // at all if the third argument is truthy.
            validateNameSyntax: function(tokens, line, genericsAllowed) {
                if(!tokens.length) throw {
                    line: line,
                    message: "nameEmpty"
                }
                
                if(tokens.length > 2) throw {
                    line: line,
                    message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                }
                
                if(tokens.length == 1 && tokens[0] == "?") throw {
                    line: line,
                    message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                }
                
                if(tokens.length > 1) {
                    if(tokens[0] != "?" || tokens[1] == "?") throw {
                        line: line,
                        message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                    }
                        
                    if(!genericsAllowed) throw {
                        line: line,
                        message: "genericTypeUsedInNonGenericContext"
                    }
                }
                
                if(tokens[0] == "?" && !genericsAllowed) throw {
                    line: line,
                    message: "genericTypeUsedInNonGenericContext"
                }                
                
                for(var tokenId = 0; tokenId < tokens.length; tokenId++) {
                    if(["let", "takes", "returns", "combines", "input"].indexOf(tokens[tokenId]) != -1) throw {
                        line: line,
                        message: "nameExpectedKeywordGiven"
                    }
                    
                    if(instance.primitives[tokens[tokenId]]) throw {
                        line: line,
                        message: "nameExpectedPrimitiveTypeGiven"
                    }
                    
                    if(instance.getPrimitiveTypeNameByLiteral(tokens[tokenId]))
                        throw {
                            line: line,
                            message: "namesMayNotContainPrimitiveLiterals"
                        }
                }
            },
            
            // Given the name of a file and the content of the file, returns an
            // array of objects containing:
            //  filename: the source filename.
            //  line: one-indexed line number from the start of the file.
            // Only one of the following will be included:
            //  indent: truthy when the next line is further indented.
            //  unindent: truthy when the next line is one level less indented.
            //  tokens: an array of the tokens on this line.
            // Should an error occur, an object will be thrown containing:
            //  message: one of the following strings:
            //   unindentedToUnexpectedLevel: a line was unindented to a level
            //    which was not previously indented to.
            //  line: one-indexed line number from the start of the file.
            tokenize: function(filename, file) {
                var output = []
                var lines = file.split("\n")
                var previousWhitespaces = []
                for(var lineId = 0; lineId < lines.length; lineId++) {
                    var line = lines[lineId]
                    
                    // Find the first instance of a non-whitespace character.
                    var whitespaces = line.search(/\S/)
                    
                    // Ignore lines which contain only whitespace, a comment or nothing at all.
                    if(whitespaces == -1) continue
                    if(line.charAt(whitespaces) == "#") continue
                    
                    // If this number of whitespaces isn't recognized we need to either indent or unindent.
                    if(whitespaces && previousWhitespaces.indexOf(whitespaces) == -1) {
                        
                        // If we've gone back in to an unrecognized level we don't know what to do.
                        if(whitespaces < previousWhitespaces[previousWhitespaces.length - 1])
                            throw {
                                message: "unindentedToUnexpectedLevel",
                                line: lineId + 1,
                                filename: filename
                            }
                            
                        previousWhitespaces.push(whitespaces)
                        
                        output.push({
                            filename: filename,
                            line: lineId + 1,
                            indent: true
                        })
                    }
                    
                    // If we have whitespaces but are too high, unindent until we are where we should be.
                    if(previousWhitespaces.indexOf(whitespaces) != -1)
                        while(previousWhitespaces.indexOf(whitespaces) != previousWhitespaces.length - 1) {
                            previousWhitespaces.length--
                            
                            output.push({
                                filename: filename,
                                line: lineId + 1,
                                unindent: true
                            })
                        }
                        
                    // If we have no whitespaces, unindent all the way.
                    if(!whitespaces)
                        while(previousWhitespaces.length) {
                            previousWhitespaces.length--
                            
                            output.push({
                                filename: filename,
                                line: lineId + 1,
                                unindent: true
                            })
                        }
                        
                    // Write the line's tokens.
                    output.push({
                        filename: filename,
                        line: lineId + 1,
                        tokens: line.trim().split(/\s+/g)
                    })
                }
                
                // Insert unindents if the end of the file was indented.
                while(previousWhitespaces.length) {
                    output.push({
                        filename: filename,
                        line: lines.length + 1,
                        unindent: true
                    })
                    previousWhitespaces.length--
                }
                
                return output
            },
            
            // Given a type reference, the name of what may be a field in that
            // type reference, and the parsed types, returns falsy if none
            // exists, otherwise returning an object:
            //   field: The field object.
            //   of: The "real" type of the field based on the generic given in
            //       the type reference.
            findFieldByInputAndName: function(typeReference, name, parsed) {
                if(typeReference.length == 1) {
                    var combination = parsed.combinations[typeReference[0]]
                    if(!combination) return
                    var field = combination.fields[name]
                    if(!field) return
                    return {
                        field: field,
                        of: field.of
                    }
                } else {
                    var combination = parsed.genericCombinations[typeReference[typeReference.length - 1]]
                    if(!combination) return
                    var field = combination.fields[name]
                    if(!field) return
                    return {
                        field: field,
                        of: field.of[0] == "?" ? typeReference.slice(0, typeReference.length - 1).concat(field.of.slice(1)) : field.of
                    }
                }
            },
            
            // Given a list of tokens generated by "tokenize", parses through
            // the tokens and finds declarations of types and functions, but
            // does not perform any type checking.  Returns an object containing
            // the following:
            //  aliases/genericAliases: an object where the keys are the alias 
            //           names and the values are objects describing type 
            //           aliases:
            //      line: The object from the input which declared the 
            //                alias.
            //      of: An array of the type names aliased.
            //  combinations/genericCombinations: an object where the keys are 
            //                the combination names and the values are objects 
            //                describing combination types:
            //      line: The object from the input which declared the 
            //            combination.
            //      fields: An object where the keys are the field names and
            //              the values are objects describing fields:
            //           line: The object from the input which declared the 
            //            field.
            //           of: An array of the type names held by the field.
            //      fieldsOrder: An array of the names of the fields, in the
            //                   order they were declared.
            //  functions: an array of objects describing functions:
            //      name: The name of the function.
            //      line: The object from the input which declared the function.
            //      takes: An array of the type names taken as input.
            //      returns: An array of the type names returned.
            //      lets: An object where the keys are the names of lets within
            //            the function and the values are objects describing
            //            them:
            //         line: The object from the input which declared the let.
            //         returns: The type stored by the let.  See below.
            //         fields: An array of the chains which generate the fields
            //                 of the return type, in order.
            //      fields: An array of the chains which generate the fields of
            //              the return type, in order.  See below.
            //    Chains are objects containing the following:
            //      line: The object from the input which declared the 
            //            chain.
            //      functions: An array of the function names applied to the
            //                 value.
            //      Only one of the following is truthy, specifying the source
            //      of the value:
            //        primitive: The token interpreted as a literal.
            //        let: The token interpreted as the name of a let.
            //        input: Truthy when "input" was given.
            // Should an error occur, an object will be thrown containing:
            //  message: one of the following strings:
            //    aliasNamesNotUnique
            //    aliasesRequireOneName
            //    aliasesRequireOneOrMoreTypeNames
            //    onlyOneAliasCanBeCreatedPerLine
            //  line: A reference to the object in the lines given which
            //        generated the error.
            parse: function(tokenized) {
                var output = {
                    genericAliases: {},
                    aliases: {},
                    combinations: {},
                    genericCombinations: {},
                    functions: []
                }
                
                var declaringCombination = null, declaringFunction = null, declaringLet = null, indents = 0, generic
                
                for(var lineId = 0; lineId < tokenized.length; lineId++) {
                    var line = tokenized[lineId]
                    
                    if(declaringFunction) {
                        if(line.indent) {
                            if(indents && !declaringLet || indents == 2) throw {
                                message: "unexpectedIndentation",
                                line: line
                            }
                            indents++
                            continue
                        }
                        
                        if(line.unindent) {
                            indents--
                            if(indents) {
                                if(!declaringLet.fields.length) throw {
                                    message: "letEmpty",
                                    line: declaringLet.line
                                }
                                declaringLet = null
                            } else {
                                if(declaringLet) throw {
                                    message: "letEmpty",
                                    line: declaringLet.line
                                }
                                
                                if(!declaringFunction.fields.length) throw {
                                    message: "functionEmpty",
                                    line: declaringFunction.line
                                }
                                
                                declaringFunction = null
                            }
                                
                            continue
                        }
                        
                        switch(indents) {
                            case 0:
                                throw {
                                    message: "functionEmpty",
                                    line: declaringFunction.line
                                }
                            
                            case 1:
                                if(declaringLet) throw {
                                    message: "letEmpty",
                                    line: declaringLet.line
                                }
                                
                                if(line.tokens[0] == "let") {
                                    instance.validateNameSyntax(line.tokens.slice(1, 2), line, false)
                                    instance.validateTypeReferenceSyntax(line.tokens.slice(2), line, generic)
                                    
                                    if(declaringFunction.lets[line.tokens[1]]) throw {
                                        message: "letNamesNotUnique",
                                        line: line
                                    }
                                    
                                    declaringLet = declaringFunction.lets[line.tokens[1]] = {
                                        line: line,
                                        returns: line.tokens.slice(2),
                                        fields: []
                                    }
                                    continue
                                }
                                declaringFunction.fields.push(instance.validateAndCreateChain(line.tokens, line))
                                continue
                                
                            case 2:
                                declaringLet.fields.push(instance.validateAndCreateChain(line.tokens, line))
                                continue
                        }
                    }
                    
                    if(declaringCombination) {
                        if(!indents && !line.indent) throw {
                            line: declaringCombination.line,
                            message: "combinationEmpty"
                        }
                        
                        if(line.indent) {
                            if(indents) throw {
                                line: line,
                                message: "unexpectedIndentation"
                            }
                            indents = 1
                            continue
                        }
                        if(line.unindent) {
                            indents = 0
                            declaringCombination = null
                            continue
                        }
                        
                        instance.validateNameSyntax(line.tokens.slice(0, 1), line, false)
                        instance.validateTypeReferenceSyntax(line.tokens.slice(1), line, generic)
                        
                        if(declaringCombination.fields[line.tokens[0]]) throw {
                            line: line,
                            message: "fieldNamesNotUnique"
                        }
                        
                        declaringCombination.fieldsOrder.push(line.tokens[0])
                        declaringCombination.fields[line.tokens[0]] = {
                            line: line,
                            of: line.tokens.slice(1)
                        }
                        
                        continue
                    }
                    
                    if(line.indent) throw {
                        line: line,
                        message: "unexpectedIndentation"
                    }
                    
                    var aliasId = line.tokens.indexOf("aliases")
                    if(aliasId != -1) {
                        instance.validateNameSyntax(line.tokens.slice(0, aliasId), line, true)
                        generic = line.tokens[0] == "?"
                        instance.validateTypeReferenceSyntax(line.tokens.slice(aliasId + 1), line, generic)
                        
                        if(generic && line.tokens[aliasId + 1] != "?") throw {
                            message: "genericAliasOfNonGenericType",
                            line: line
                        }
                        
                        var name = generic ? line.tokens[1] : line.tokens[0]
                        var target = generic ? output.genericAliases : output.aliases
                        
                        if(target[name]) throw {
                            message: "aliasNamesNotUnique",
                            line: line
                        }
                        
                        target[name] = {
                            line: line,
                            of: line.tokens.slice(aliasId + 1)
                        }
                        continue
                    }
                    
                    var combinesId = line.tokens.indexOf("combines")
                    if(combinesId != -1) {
                        if(combinesId != line.tokens.length - 1) throw {
                            message: "unexpectedTokensFollowingCombines",
                            line: line
                        }                        
                        
                        instance.validateNameSyntax(line.tokens.slice(0, combinesId), line, true)
                        generic = line.tokens[0] == "?"
                        
                        var name = generic ? line.tokens[1] : line.tokens[0]
                        var target = generic ? output.genericCombinations : output.combinations
                        
                        if(target[name]) throw {
                            message: "combinationNamesNotUnique",
                            line: line
                        }                        
                        
                        declaringCombination = target[name] = {
                            line: line,
                            fields: {},
                            fieldsOrder: []
                        }
                        
                        continue
                    }
                    
                    var takesId = line.tokens.indexOf("takes")
                    var returnsId = line.tokens.indexOf("returns")
                    
                    if(takesId != -1 && returnsId != -1 && returnsId > takesId) {
                        instance.validateNameSyntax(line.tokens.slice(0, takesId), line, false)
                        instance.validateTypeReferenceSyntax(line.tokens.slice(takesId + 1, returnsId), line, true)
                        generic = line.tokens[takesId + 1] == "?"
                        instance.validateTypeReferenceSyntax(line.tokens.slice(returnsId + 1), line, generic)
                        
                        output.functions.push(declaringFunction = {
                            name: line.tokens[0],
                            line: line,
                            takes: line.tokens.slice(takesId + 1, returnsId),
                            returns: line.tokens.slice(returnsId + 1),
                            lets: {},
                            fields: []
                        })
                        
                        continue
                    }
                    
                    throw {
                        line: line,
                        message: "unableToDetermineIntent"
                    }
                }
                
                if(declaringFunction && !indents) throw {
                    message: "functionEmpty",
                    line: declaringFunction.line
                }
                
                if(declaringCombination && !indents) throw {
                    line: declaringCombination.line,
                    message: "combinationEmpty"
                }
                
                return output
            },
            
            // Expands every aliased type in a given parsed set of types.
            expandAllAliases: function(parsed) {
                for(var combinationId in parsed.combinations)
                    for(var fieldId in parsed.combinations[combinationId].fields)
                        instance.expandAliases(parsed.combinations[combinationId].fields[fieldId].of, parsed)
                        
                for(var combinationId in parsed.genericCombinations)
                    for(var fieldId in parsed.genericCombinations[combinationId].fields)
                        instance.expandAliases(parsed.genericCombinations[combinationId].fields[fieldId].of, parsed)                        
                        
                var expandFunction = function(funct) {
                    instance.expandAliases(funct.takes, parsed)
                    instance.expandAliases(funct.returns, parsed)
                    
                    for(var letId in funct.lets)
                        instance.expandAliases(funct.lets[letId].returns, parsed)
                }
                        
                for(var functionId in parsed.functions) {
                    expandFunction(parsed.functions[functionId])
                }
                
                for(var functionId in parsed.genericFunctions) {
                    expandFunction(parsed.genericFunctions[functionId])
                }                
            },
            
            // Given two type references, returns the generic type reference
            // if they could match.
            // i.e.:
            //    float -> float -> []
            //    float -> int -> false
            //    float -> ? -> [float]
            //    combo -> float -> false
            //    combo -> combo -> []
            //    combo1 -> combo2 -> []
            //    combo -> ? -> [combo]
            //    ? combo -> ? -> [?]
            //    ? combo -> float -> false
            //    ? combo1 -> combo2 -> false
            //    ? combo1 -> int combo1 -> [int]
            //    bool combo1 -> int combo1 -> false
            typeReferencesMatch: function(a, b) {
                
                // These always match.
                if(a.length == 1 && a[0] == "?") return b
                if(b.length == 1 && b[0] == "?") return a
                
                // If the first is a generic, see if everything after in the
                // first matches up in the second, from the top of the type
                // stack down.  If we reach the first's "?" again, that's fine.
                if(a[0] == "?") {
                    if(b.length < a.length) return
                    for(var i = 1; i < a.length; i++)
                        if(a[a.length - i] != b[b.length - i]) return
                    return b.slice(0, 1 + b.length - a.length)
                }
                
                // And again, vice-versa.
                if(b[0] == "?") {
                    if(a.length < b.length) return
                    for(var i = 1; i < b.length; i++)
                        if(a[a.length - i] != b[b.length - i]) return
                    return a.slice(0, 1 + a.length - b.length)
                }                
                
                // Nothing is generic now, so do a simple token match.
                if(a.length != b.length) return
                
                for(var i = 0; i < a.length; i++)
                    if(a[i] != b[i]) return

                return []
            },
            
            // Given a set of parsed types, checks that there are no overlapping
            // combination, alias or function declarations; that regardless, it
            // should never be ambiguous as to what is meant by a type or
            // function reference.
            checkUniqueness: function(parsed) {
                for(var key in parsed.genericAliases)
                    if(parsed.genericCombinations[key]) throw {
                        message: "typeNamesNotUnique",
                        line: parsed.genericAliases[key].line
                    }
                    
                for(var key in parsed.aliases)
                    if(parsed.combinations[key]) throw {
                        message: "typeNamesNotUnique",
                        line: parsed.aliases[key].line
                    }                    
            },
            
            // Given a set of parsed types, with aliases expanded, checks that
            // no functions have been declared with the same names and input
            // types.  Additionally ensures that no fields could clash.
            checkFunctionsForClashes: function(expanded) {
                var run = function(functions) {
                    for(var combinationId in expanded.combinations) {
                        var combination = expanded.combinations[combinationId]
                        for(var i = 0; i < functions.length; i++) {
                            var fi = functions[i]
                            if(combination.fields[fi.name] && instance.typeReferencesMatch([combinationId], fi.takes)) throw {
                                message: "fieldAndFunctionNameShareNameAndApplicableTypes",
                                line: fi.line
                            }
                        }
                    }
                    
                    for(var combinationId in expanded.genericCombinations) {
                        var combination = expanded.genericCombinations[combinationId]
                        for(var i = 0; i < functions.length; i++) {
                            var fi = functions[i]
                            if(combination.fields[fi.name] && instance.typeReferencesMatch(["?", combinationId], fi.takes)) throw {
                                message: "fieldAndFunctionNameShareNameAndApplicableTypes",
                                line: fi.line
                            }
                        }
                    }                
                }
                run(expanded.functions)
                run(instance.nativeFunctions)
                
                var crossCheck = function(a, b) {
                    for(var i = 0; i < a.length; i++) {
                        var fi = a[i]
                        for(var j = 0; j < b.length; j++) {
                            var fj = b[j]
                            if(fi == fj) continue
                            if(fi.name != fj.name) continue
                            if(instance.typeReferencesMatch(fi.takes, fj.takes)) throw {
                                message: "functionNamesNotUniqueToInputType",
                                line: fi.line
                            }
                        }
                    }                    
                }
                crossCheck(expanded.functions, expanded.functions)
                crossCheck(expanded.functions, instance.nativeFunctions)
                crossCheck(instance.nativeFunctions, instance.nativeFunctions)
            },
            
            // Given a parsed set of types, ensures that no aliases recurse
            // infinitely.
            checkForAliasLoops: function(parsed) {
                var recurseChain = function(key, stack) {
                    if(stack.indexOf(key) != -1) throw {
                        message: "infiniteLoopInGenericAlias",
                        line: parsed.genericAliases[key].line
                    }
                    if(parsed.genericAliases[key]) {
                        stack.push(key)
                        for(var tokenId = 0; tokenId < parsed.genericAliases[key].of.length; tokenId++)
                            recurseChain(parsed.genericAliases[key].of[tokenId], stack.slice())
                    }
                }
                for(var key in parsed.genericAliases) recurseChain(key, [])
                
                var recurseBase = function(key, stack) {
                    if(stack.indexOf(key) != -1) throw {
                        message: "infiniteLoopInNonGenericAlias",
                        line: parsed.aliases[key].line
                    }
                    if(parsed.aliases[key]) {
                        stack.push(key)
                        recurseBase(parsed.aliases[key].of[0], stack)
                    }
                }
                for(var key in parsed.aliases) recurseBase(key, [])
            },
            
            // Given a type reference, the source line and the parsed types, 
            // ensures that every part of the type reference can be resolved as 
            // a type.
            validateTypeReference: function(typeReference, line, parsed) {
                if(!(typeReference[0] == "?"
                    || parsed.combinations[typeReference[0]] 
                    || instance.primitives[typeReference[0]]
                    || parsed.aliases[typeReference[0]])) throw {
                        message: "couldNotMatchBaseType",
                        line: line
                    }

                for(var tokenId = 1; tokenId < typeReference.length; tokenId++)
                    if(!(parsed.genericCombinations[typeReference[tokenId]]
                        || parsed.genericAliases[typeReference[tokenId]])) throw {
                            message: "couldNotMatchChainType",
                            line: line
                        }
            },
            
            // Ensures that every type reference in a given set of parsed types
            // is resolvable.
            validateAllTypeReferences: function(parsed) {
                for(var combinationId in parsed.aliases)
                    instance.validateTypeReference(parsed.aliases[combinationId].of, parsed.aliases[combinationId].line, parsed)
                        
                for(var combinationId in parsed.genericAliases)
                    instance.validateTypeReference(parsed.genericAliases[combinationId].of, parsed.genericAliases[combinationId].line, parsed)      
                
                for(var combinationId in parsed.combinations)
                    for(var fieldId in parsed.combinations[combinationId].fields)
                        instance.validateTypeReference(parsed.combinations[combinationId].fields[fieldId].of, parsed.combinations[combinationId].fields[fieldId].line, parsed)
                        
                for(var combinationId in parsed.genericCombinations)
                    for(var fieldId in parsed.genericCombinations[combinationId].fields)
                        instance.validateTypeReference(parsed.genericCombinations[combinationId].fields[fieldId].of, parsed.genericCombinations[combinationId].fields[fieldId].line, parsed)                        
                 
                var validateFunction = function(funct) {
                    instance.validateTypeReference(funct.takes, funct.line, parsed)
                    instance.validateTypeReference(funct.returns, funct.line, parsed)
                    
                    for(var letId in funct.lets)
                        instance.validateTypeReference(funct.lets[letId].returns, funct.lets[letId].line, parsed)
                }
                        
                for(var functionId in parsed.functions) {
                    validateFunction(parsed.functions[functionId])
                }
                
                for(var functionId in parsed.genericFunctions) {
                    validateFunction(parsed.genericFunctions[functionId])
                }                
            },
            
            // Given a set of parsed types with their aliases expanded, ensures
            // that there are no loops in the combination types.  Expects all
            // type references to have been validated.
            checkForCombinationLoops: function(expanded) {
                var check = function(combination, stack, line) {
                    if(stack.indexOf(combination) != -1) throw {
                        message: "infiniteLoopInCombination",
                        line: line
                    }
                    var nextStack = stack.slice()
                    nextStack.push(combination)
                    for(var fieldId in combination.fields) {
                        var field = combination.fields[fieldId]
                        if(expanded.combinations[field.of[0]])
                            check(expanded.combinations[field.of[0]], nextStack, field.line)
                        for(var tokenId = 1; tokenId < field.of.length; tokenId++) {
                            var token = field.of[tokenId]
                            if(expanded.genericCombinations[token])
                                check(expanded.genericCombinations[token], nextStack, field.line)
                        }
                    }
                }
                
                for(var combinationId in expanded.combinations)
                    check(expanded.combinations[combinationId], [], expanded.combinations[combinationId].line)
                    
                for(var combinationId in expanded.genericCombinations)
                    check(expanded.genericCombinations[combinationId], [], expanded.genericCombinations[combinationId].line)                    
            },
            
            expandAliases: function(typeReference, parsed) {
                var alias = parsed.aliases[typeReference[0]]
                if(alias) {
                    typeReference.shift()
                    for(var tokenId = alias.of.length - 1; tokenId >= 0; tokenId--)
                        typeReference.unshift(alias.of[tokenId])
                    instance.expandAliases(typeReference, parsed)
                    return
                }
                
                for(var tokenId = 1; tokenId < typeReference.length; tokenId++) {
                    var alias = parsed.genericAliases[typeReference[tokenId]]
                    if(alias) {
                        typeReference.splice(tokenId, 1)
                        for(var subtokenId = 1; subtokenId < alias.of.length; subtokenId++)
                            typeReference.splice(tokenId++, 0, alias.of[subtokenId])
                        instance.expandAliases(typeReference, parsed)
                        return
                    }
                }
            },
            
            // Given a type reference, the name of a function and a set of 
            // parsed types, attempts to find a funciton matching, returning
            // falsy if none exists, otherwise returning an object:
            //   function: A reference to function matched.
            //   takes: The real input type, with any generics subsituted.
            //   returns: The real return type, with any generics substituted.
            // Assumes that checkFunctionsForClashes has been called.
            findFunctionByInputAndName: function(input, name, parsed) {
                var search = function(functions) {
                    for(var functionId = 0; functionId < functions.length; functionId++) {
                        var funct = functions[functionId]
                        if(funct.name != name) continue
                        var match = instance.typeReferencesMatch(funct.takes, input)
                        if(!match) continue
                        return {
                            function: funct,
                            takes: funct.takes[0] == "?" ? match.concat(funct.takes.slice(1)) : funct.takes,
                            returns: funct.returns[0] == "?" ? match.concat(funct.returns.slice(1)) : funct.returns
                        }
                    }
                }
                return search(parsed.functions) || search(instance.nativeFunctions)
            },
            
            // Given a type reference and a set of parsed types, finds and
            // returns the fields that type reference refers to.  This will
            // automatically substitute any open generics, so a type reference
            // of "int someGeneric" will replace the ? in someGeneric with int.
            // Assumes all type references have been validated.
            getFieldsByTypeReference: function(typeReference, parsed) {
                var output, combination
                if(typeReference.length == 1) {
                    if(typeReference[0] == "?" || instance.primitives[typeReference[0]]) return [typeReference]
                    output = []
                    combination = parsed.combinations[typeReference[0]]
                    for(var fieldId = 0; fieldId < combination.fieldsOrder.length; fieldId++)
                        output.push(combination.fields[combination.fieldsOrder[fieldId]].of)
                    return output
                }
                
                combination = parsed.genericCombinations[typeReference[typeReference.length - 1]]
                
                output = []
                for(var fieldId = 0; fieldId < combination.fieldsOrder.length; fieldId++) {
                    var original = combination.fields[combination.fieldsOrder[fieldId]].of
                    if(original[0] == "?") 
                        output.push(typeReference.slice(0, typeReference.length - 1).concat(original.slice(1)))
                    else
                        output.push(original)
                }
                return output
            },
            
            // Builds a given object describing a file to build, containing:
            //   files: An array of objects describing the source to build:
            //     source: A string containing the source code.
            //     filename: The filename as a string; used only for error 
            //               reporting.
            // The second argument is a callback to execute at the start of each
            // stage.  It takes the following arguments:
            //   name: The name of the stage.
            //   completed: The number of completed stages.
            //   total: The total number of stages.
            // The third argument is a callback to execute at the end of each
            // stage.  It takes the following arguments:
            //   name: The name of the stage.
            //   completed: The number of completed stages.
            //   total: The total number of stages.
            // The fourth argument is a callback to execute on total success.
            // It takes the following arguments:
            //   result: The built source code.
            //   runtime: The number of milliseconds spent building.
            // The fifth argument is a callback to execute on an expected 
            // exception.  It takes the following arguments:
            //   line: The line object; usually contains:
            //     filename: String, given in input.
            //     line: One-based line number from the start of that file.
            //     tokens: An array of the tokens on that line.
            //   message: The error message.  This is a string.
            //   runtime: The number of milliseconds spent building.
            //   completed: The number of completed stages.
            //   total: The total number of stages.            
            // Unexpected exceptions are bubbled up.
            build: function(input, stepStart, stepEnd, success, failure) {
                var start = Date.now()
                for(var stepId = 0; stepId < instance.buildSteps.length; stepId++) {
                    var step = instance.buildSteps[stepId]
                    try {
                        stepStart(step.name, stepId, instance.buildSteps.length)
                        input = step.perform(input)
                        stepEnd(step.name, stepId + 1, instance.buildSteps.length)
                    } catch(e) {
                        if(e.message && e.line) {
                            failure(e.line, e.message, Date.now() - start, stepId, instance.buildSteps.length)
                            return
                        }
                        throw e
                    }
                }
                
                success(input, Date.now() - start)
            },
            
            // An array of the steps iterated through by build in order.
            // Each is an object containing the following:
            //    name: Used to identify the stage in messages/etc.
            //    perform: Called with the output of the previous stage or the
            //             build input as a whole to perform that stage.
            //             Exceptions containing a line and message will be
            //             handled.
            buildSteps: [{
                name: "tokenize",
                perform: function(input){
                    var lines = []
                    for(var fileId = 0; fileId < input.length; fileId++) {
                        lines = lines.concat(instance.tokenize(input[fileId].filename, input[fileId].source))
                    }
                    return lines
                }
            }, {
                name: "parse",
                perform: function(input){
                    return instance.parse(input)
                }
            }, {
                name: "checkUniqueness",
                perform: function(input){
                    instance.checkUniqueness(input)
                    return input
                }
            }, {
                name: "checkTypesResolvable",
                perform: function(input){
                    instance.validateAllTypeReferences(input)
                    return input
                }
            }, {
                name: "checkAliases",
                perform: function(input){
                    instance.checkForAliasLoops(input)
                    return input
                }
            }, {
                name: "expandAliases",
                perform: function(input){
                    instance.expandAllAliases(input)
                    return input
                }
            }, {
                name: "checkForCombinationLoops",
                perform: function(input){
                    instance.checkForCombinationLoops(input)
                    return input
                }
            }, {
                name: "checkFunctionsForClashes",
                perform: function(input){
                    instance.checkFunctionsForClashes(input)
                    return input
                }
            }, {
                name: "checkFunctionLets",
                perform: function(input){
                    for(var functionId = 0; functionId < input.functions.length; functionId++) {
                        instance.checkFunctionLets(input.functions[functionId], input)
                    }
                    return input
                }
            }, {
                name: "checkFunctionFields",
                perform: function(input){
                    for(var functionId = 0; functionId < input.functions.length; functionId++) {
                        instance.typecheckFieldChains(input.functions[functionId], input.functions[functionId], input)
                    }
                    return input
                }
            }]
        }
        return instance
    }
}

SUNRUSE.influx.compilers = {}