describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("validateTypeReference", function(){
            var instance, types
            beforeEach(function(){
                instance = SUNRUSE.influx()
                types = {
                    genericAliases: {
                        genericAliasA: {},
                        genericAliasB: {}
                    },
                    aliases: {
                        aliasA: {},
                        aliasB: {}
                    },
                    combinations: {
                        combinationA: {},
                        combinationB: {}
                    },
                    genericCombinations: {
                        genericCombinationA: {},
                        genericCombinationB: {}
                    }
                }
                instance.primitives = {
                    primitiveA: {
                        regex: /^valueFromPrimitiveA$/
                    },
                    primitiveB: {
                        regex: /^valueFromPrimitiveB$/
                    },
                    primitiveC: {
                        regex: /^valueFromPrimitiveC$/
                    }                   
                }                                
            })
            
            it("does nothing given ?", function(){
                instance.validateTypeReference(["?"], "Test Line A", types)
            })
            
            it("does nothing given a primitive type", function(){
                instance.validateTypeReference(["primitiveB"], "Test Line A", types)
            })
            
            it("does nothing given a nongeneric combination as a base", function(){
                instance.validateTypeReference(["combinationA"], "Test Line A", types)
            })
            
            it("does nothing given a nongeneric alias as a base", function(){
                instance.validateTypeReference(["aliasA"], "Test Line A", types)
            })
            
            it("does nothing given an open generic alias", function(){
                instance.validateTypeReference(["?", "genericAliasA"], "Test Line A", types)
            })
            
            it("does nothing given a closed generic alias with a primitive", function(){
                instance.validateTypeReference(["primitiveB", "genericAliasA"], "Test Line A", types)
            })
            
            it("does nothing given a closed generic alias with a combination", function(){
                instance.validateTypeReference(["combinationA", "genericAliasA"], "Test Line A", types)
            })            
            
            it("does nothing given a closed generic alias with an alias", function(){
                instance.validateTypeReference(["aliasA", "genericAliasA"], "Test Line A", types)
            })                        
            
            it("does nothing given an open generic combination", function(){
                instance.validateTypeReference(["?", "genericCombinationA"], "Test Line A", types)
            })
            
            it("does nothing given a closed generic combination with a primitive", function(){
                instance.validateTypeReference(["primitiveB", "genericCombinationA"], "Test Line A", types)
            })
            
            it("does nothing given a closed generic combination with a combination", function(){
                instance.validateTypeReference(["combinationA", "genericCombinationA"], "Test Line A", types)
            })            
            
            it("does nothing given a closed generic combination with an alias", function(){
                instance.validateTypeReference(["aliasA", "genericCombinationA"], "Test Line A", types)
            })
            
            it("throws an exception given a generic combination as a base", function(){
                expect(function(){
                    instance.validateTypeReference(["genericCombinationA"], "Test Line A", types)
                }).toThrow({
                    message: "couldNotMatchBaseType",
                    line: "Test Line A"
                })
            })
            
            it("throws an exception given a nongeneric combination in a chain", function(){
                expect(function(){
                    instance.validateTypeReference(["primitiveB", "combinationA"], "Test Line A", types)
                }).toThrow({
                    message: "couldNotMatchChainType",
                    line: "Test Line A"
                })
            })
            
            it("throws an exception given a generic alias as a base", function(){
                expect(function(){
                    instance.validateTypeReference(["genericAliasA"], "Test Line A", types)
                }).toThrow({
                    message: "couldNotMatchBaseType",
                    line: "Test Line A"
                })
            })
            
            it("throws an exception given a nongeneric alias in a chain", function(){
                expect(function(){
                    instance.validateTypeReference(["primitiveB", "aliasA"], "Test Line A", types)
                }).toThrow({
                    message: "couldNotMatchChainType",
                    line: "Test Line A"
                })
            })
            
            it("throws an exception when the base is unresolvable", function(){
                expect(function(){
                    instance.validateTypeReference(["xyz", "genericCombinationA"], "Test Line A", types)
                }).toThrow({
                    message: "couldNotMatchBaseType",
                    line: "Test Line A"
                })
            })
            
            it("throws an exception when part of the chain is unresolvable", function(){
                expect(function(){
                    instance.validateTypeReference(["primitiveB", "xyz"], "Test Line A", types)
                }).toThrow({
                    message: "couldNotMatchChainType",
                    line: "Test Line A"
                })
            })
        })
    })
})