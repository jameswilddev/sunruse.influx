describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("parse", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "validateNameSyntax")
                spyOn(instance, "validateTypeReferenceSyntax")
                spyOn(instance, "validateAndCreateChain")
            })
            
            it("throws an exception on encountering unexpected indentation", function(){
                expect(function(){
                    instance.parse([{
                        filename: "Test Filename A",
                        line: 323223,
                        indent: true
                    }])
                }).toThrow({
                    line: {
                        filename: "Test Filename A",
                        line: 323223,
                        indent: true
                    },
                    message: "unexpectedIndentation"
                })
            })
            
            it("throws an exception on encountering a line containing no keywords", function(){
                expect(function(){
                    instance.parse([{
                        filename: "Test Filename A",
                        line: 323223,
                        tokens: ["micellaneous", "meaningless", "tokens"]
                    }])
                }).toThrow({
                    line: {
                        filename: "Test Filename A",
                        line: 323223,
                        tokens: ["micellaneous", "meaningless", "tokens"]
                    },
                    message: "unableToDetermineIntent"
                })
            })
            
            describe("aliases", function(){
                it("creates aliases based on those in the file", function(){
                    var result = instance.parse([{
                        filename: "test filename a",
                        line: 37,
                        tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                    }, {
                        filename: "test filename b",
                        line: 45,
                        tokens: ["?", "testAliasB", "aliases", "?", "another"]
                    }, {
                        filename: "test filename c",
                        line: 72,
                        tokens: ["?", "testAliasC", "aliases", "?", "yetAnother", "alias"]
                    }])
                    
                    expect(result.aliases).toEqual({
                        testAliasA: {
                            line: {
                                filename: "test filename a",
                                line: 37,
                                tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                            },
                            of: ["its", "type", "chain"]
                        }
                    })
                    
                    expect(result.genericAliases).toEqual({
                        testAliasB: {
                            line: {
                                filename: "test filename b",
                                line: 45,
                                tokens: ["?", "testAliasB", "aliases", "?", "another"]
                            },
                            of: ["?", "another"],
                        }, 
                        testAliasC: {
                            line: {
                                filename: "test filename c",
                                line: 72,
                                tokens: ["?", "testAliasC", "aliases", "?", "yetAnother", "alias"]
                            },
                            of: ["?", "yetAnother", "alias"]
                        }
                    })                    
                })
                
                it("validates their names", function(){
                    instance.parse([{
                        filename: "test filename a",
                        line: 37,
                        tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                    }, {
                        filename: "test filename b",
                        line: 45,
                        tokens: ["?", "testAliasB", "aliases", "?", "another"]
                    }, {
                        filename: "test filename c",
                        line: 72,
                        tokens: ["?", "testAliasC", "aliases", "?", "yetAnother", "alias"]
                    }])
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["testAliasA"], {
                        filename: "test filename a",
                        line: 37,
                        tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                    }, true)
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["?", "testAliasB"], {
                        filename: "test filename b",
                        line: 45,
                        tokens: ["?", "testAliasB", "aliases", "?", "another"]
                    }, true)             
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["?", "testAliasC"], {
                        filename: "test filename c",
                        line: 72,
                        tokens: ["?", "testAliasC", "aliases", "?", "yetAnother", "alias"]
                    }, true)                        
                })     
                
                it("validates their type references", function(){
                    instance.parse([{
                        filename: "test filename a",
                        line: 37,
                        tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                    }, {
                        filename: "test filename b",
                        line: 45,
                        tokens: ["?", "testAliasB", "aliases", "?", "another"]
                    }, {
                        filename: "test filename c",
                        line: 72,
                        tokens: ["?", "testAliasC", "aliases", "?", "yetAnother", "alias"]
                    }])
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["its", "type", "chain"], {
                        filename: "test filename a",
                        line: 37,
                        tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                    }, false)
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["?", "another"], {
                        filename: "test filename b",
                        line: 45,
                        tokens: ["?", "testAliasB", "aliases", "?", "another"]
                    }, true)             
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["?", "yetAnother", "alias"], {
                        filename: "test filename c",
                        line: 72,
                        tokens: ["?", "testAliasC", "aliases", "?", "yetAnother", "alias"]
                    }, true)       
                })

                it("throws an exception on trying to define a partially generic alias", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "test filename a",
                            line: 37,
                            tokens: ["?", "thing", "aliases", "a", "nonGeneric"]
                        }])
                    }).toThrow({
                        message: "genericAliasOfNonGenericType",
                        line: {
                            filename: "test filename a",
                            line: 37,
                            tokens: ["?", "thing", "aliases", "a", "nonGeneric"]
                        }
                    })
                })
                
                it("throws an exception on encountering two aliases with the same name", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "test filename a",
                            line: 37,
                            tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                        }, {
                            filename: "test filename b",
                            line: 45,
                            tokens: ["testAliasA", "aliases", "another"]
                        }])
                    }).toThrow({
                        message: "aliasNamesNotUnique",
                        line: {
                            filename: "test filename b",
                            line: 45,
                            tokens: ["testAliasA", "aliases", "another"]
                        }
                    })
                })
                
                it("throws an exception on encountering two generic aliases with the same name", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "test filename a",
                            line: 37,
                            tokens: ["?", "testAliasA", "aliases", "?", "its", "type", "chain"]
                        }, {
                            filename: "test filename b",
                            line: 45,
                            tokens: ["?", "testAliasA", "aliases", "?", "another"]
                        }])
                    }).toThrow({
                        message: "aliasNamesNotUnique",
                        line: {
                            filename: "test filename b",
                            line: 45,
                            tokens: ["?", "testAliasA", "aliases", "?", "another"]
                        }
                    })
                })                
                
                it("allows generic and non-generic aliases with the same name", function(){
                    instance.parse([{
                        filename: "test filename a",
                        line: 37,
                        tokens: ["testAliasA", "aliases", "its", "type", "chain"]
                    }, {
                        filename: "test filename b",
                        line: 45,
                        tokens: ["?", "testAliasA", "aliases", "?", "another"]
                    }])
                })                 
            })
            
            describe("combinations", function(){
                var result
                beforeEach(function(){
                    result = instance.parse([{
                        filename: "Test Filename A",
                        line: 9983,
                        tokens: ["nonGeneric", "combines"]
                    }, {
                        filename: "Test Filename B",
                        line: 887,
                        indent: true
                    }, {
                        filename: "Test Filename C",
                        line: 556,
                        tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                    }, {
                        filename: "Test Filename D",
                        line: 357,
                        tokens: ["testFieldNameAB", "testFieldTypeABA"]
                    }, {
                        filename: "Test Filename E",
                        line: 2041,
                        unindent: true
                    }, {
                        filename: "Test Filename F",
                        line: 321,
                        tokens: ["?", "generic", "combines"]
                    }, {
                        filename: "Test Filename G",
                        line: 537,
                        indent: true
                    }, {
                        filename: "Test Filename H",
                        line: 2738,
                        tokens: ["testFieldNameBA", "?", "testFieldTypeBAB"]
                    }, {
                        filename: "Test Filename I",
                        line: 75825,
                        tokens: ["testFieldNameBB", "testFieldTypeBBA"]
                    }, {
                        filename: "Test Filename J",
                        line: 19828,
                        unindent: true
                    }])                
                })
                
                it("creates combinations based on those in the file", function(){
                    expect(result.combinations).toEqual({
                        nonGeneric: {
                            line: {
                                filename: "Test Filename A",
                                line: 9983,
                                tokens: ["nonGeneric", "combines"]
                            },
                            fields: {
                                testFieldNameAA: {
                                    line: {
                                        filename: "Test Filename C",
                                        line: 556,
                                        tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                                    },
                                    of: ["testFieldTypeAAA", "testFieldTypeAAB"]
                                },
                                testFieldNameAB: {
                                    line: {
                                        filename: "Test Filename D",
                                        line: 357,
                                        tokens: ["testFieldNameAB", "testFieldTypeABA"]
                                    },
                                    of: ["testFieldTypeABA"]
                                }
                            },
                            fieldsOrder: ["testFieldNameAA", "testFieldNameAB"]
                        }
                    })
                    
                    expect(result.genericCombinations).toEqual({
                        generic: {
                            line: {
                                filename: "Test Filename F",
                                line: 321,
                                tokens: ["?", "generic", "combines"]
                            },
                            fields: {
                                testFieldNameBA: {
                                    line: {
                                        filename: "Test Filename H",
                                        line: 2738,
                                        tokens: ["testFieldNameBA", "?", "testFieldTypeBAB"]
                                    },
                                    of: ["?", "testFieldTypeBAB"]
                                },
                                testFieldNameBB: {
                                    line: {
                                        filename: "Test Filename I",
                                        line: 75825,
                                        tokens: ["testFieldNameBB", "testFieldTypeBBA"]
                                    },
                                    of: ["testFieldTypeBBA"]
                                }
                            },
                            fieldsOrder: ["testFieldNameBA", "testFieldNameBB"]
                        }
                    })                    
                })
                
                it("throws an exception when tokens follow 'combines'", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["nonGeneric", "combines", "unexpected"]
                        }, {
                            filename: "Test Filename B",
                            line: 887,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 556,
                            tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                        }, {
                            filename: "Test Filename D",
                            line: 357,
                            tokens: ["testFieldNameAB", "testFieldTypeABA"]
                        }, {
                            filename: "Test Filename E",
                            line: 2041,
                            unindent: true
                        }])                        
                    }).toThrow({
                        line: {
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["nonGeneric", "combines", "unexpected"]
                        },
                        message: "unexpectedTokensFollowingCombines"
                    })
                })
                
                it("throws an exception when generic combinations are not uniquely named", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["?", "conflict", "combines"]
                        }, {
                            filename: "Test Filename B",
                            line: 887,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 556,
                            tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                        }, {
                            filename: "Test Filename D",
                            line: 357,
                            tokens: ["testFieldNameAB", "testFieldTypeABA"]
                        }, {
                            filename: "Test Filename E",
                            line: 2041,
                            unindent: true
                        }, {
                            filename: "Test Filename F",
                            line: 321,
                            tokens: ["?", "conflict", "combines"]
                        }, {
                            filename: "Test Filename G",
                            line: 537,
                            indent: true
                        }, {
                            filename: "Test Filename H",
                            line: 2738,
                            tokens: ["testFieldNameBA", "?", "testFieldTypeBAB"]
                        }, {
                            filename: "Test Filename I",
                            line: 75825,
                            tokens: ["testFieldNameBB", "testFieldTypeBBA"]
                        }, {
                            filename: "Test Filename J",
                            line: 19828,
                            unindent: true
                        }])                        
                    }).toThrow({
                        line: {
                            filename: "Test Filename F",
                            line: 321,
                            tokens: ["?", "conflict", "combines"]
                        },
                        message: "combinationNamesNotUnique"
                    })
                })
                
                it("throws an exception when non-generic combinations are not uniquely named", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["conflict", "combines"]
                        }, {
                            filename: "Test Filename B",
                            line: 887,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 556,
                            tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                        }, {
                            filename: "Test Filename D",
                            line: 357,
                            tokens: ["testFieldNameAB", "testFieldTypeABA"]
                        }, {
                            filename: "Test Filename E",
                            line: 2041,
                            unindent: true
                        }, {
                            filename: "Test Filename F",
                            line: 321,
                            tokens: ["conflict", "combines"]
                        }, {
                            filename: "Test Filename G",
                            line: 537,
                            indent: true
                        }, {
                            filename: "Test Filename H",
                            line: 2738,
                            tokens: ["testFieldNameBA", "testFieldTypeBAB"]
                        }, {
                            filename: "Test Filename I",
                            line: 75825,
                            tokens: ["testFieldNameBB", "testFieldTypeBBA"]
                        }, {
                            filename: "Test Filename J",
                            line: 19828,
                            unindent: true
                        }])                        
                    }).toThrow({
                        line: {
                            filename: "Test Filename F",
                            line: 321,
                            tokens: ["conflict", "combines"]
                        },
                        message: "combinationNamesNotUnique"
                    })
                })
                
                it("allows non-generic and generic combinations with the same name", function(){
                    instance.parse([{
                        filename: "Test Filename A",
                        line: 9983,
                        tokens: ["noConflict", "combines"]
                    }, {
                        filename: "Test Filename B",
                        line: 887,
                        indent: true
                    }, {
                        filename: "Test Filename C",
                        line: 556,
                        tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                    }, {
                        filename: "Test Filename D",
                        line: 357,
                        tokens: ["testFieldNameAB", "testFieldTypeABA"]
                    }, {
                        filename: "Test Filename E",
                        line: 2041,
                        unindent: true
                    }, {
                        filename: "Test Filename F",
                        line: 321,
                        tokens: ["?", "noConflict", "combines"]
                    }, {
                        filename: "Test Filename G",
                        line: 537,
                        indent: true
                    }, {
                        filename: "Test Filename H",
                        line: 2738,
                        tokens: ["testFieldNameBA", "?", "testFieldTypeBAB"]
                    }, {
                        filename: "Test Filename I",
                        line: 75825,
                        tokens: ["testFieldNameBB", "testFieldTypeBBA"]
                    }, {
                        filename: "Test Filename J",
                        line: 19828,
                        unindent: true
                    }])                    
                })
                
                it("throws an exception when no fields are present at the end of the file", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["conflict", "combines"]
                        }])                        
                    }).toThrow({
                        line: {
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["conflict", "combines"]
                        },
                        message: "combinationEmpty"
                    })
                })
                
                it("throws an exception when no fields are present when followed by a new declaration", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["conflict", "combines"]
                        }, {
                            filename: "Test Filename F",
                            line: 321,
                            tokens: ["?", "generic", "combines"]
                        }])                        
                    }).toThrow({
                        line: {
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["conflict", "combines"]
                        },
                        message: "combinationEmpty"
                    })
                })                
                
                it("throws an exception when fields are not uniquely named", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["nonGeneric", "combines"]
                        }, {
                            filename: "Test Filename B",
                            line: 887,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 556,
                            tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                        }, {
                            filename: "Test Filename D",
                            line: 357,
                            tokens: ["testFieldNameAA", "testFieldTypeABA"]
                        }, {
                            filename: "Test Filename E",
                            line: 2041,
                            unindent: true
                        }])
                    }).toThrow({
                        message: "fieldNamesNotUnique",
                        line: {
                            filename: "Test Filename D",
                            line: 357,
                            tokens: ["testFieldNameAA", "testFieldTypeABA"]
                        }                        
                    })                 
                })
                
                it("validates the names of the combinations", function(){
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["nonGeneric"], {
                        filename: "Test Filename A",
                        line: 9983,
                        tokens: ["nonGeneric", "combines"]
                    }, true)
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["?", "generic"], {
                        filename: "Test Filename F",
                        line: 321,
                        tokens: ["?", "generic", "combines"]
                    }, true)                    
                })
                
                it("validates the names of their fields", function(){
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["testFieldNameAA"], {
                        filename: "Test Filename C",
                        line: 556,
                        tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                    }, false)
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["testFieldNameAB"], {
                        filename: "Test Filename D",
                        line: 357,
                        tokens: ["testFieldNameAB", "testFieldTypeABA"]
                    }, false)                    
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["testFieldNameBA"], {
                        filename: "Test Filename H",
                        line: 2738,
                        tokens: ["testFieldNameBA", "?", "testFieldTypeBAB"]
                    }, false)                                        
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["testFieldNameBB"], {
                        filename: "Test Filename I",
                        line: 75825,
                        tokens: ["testFieldNameBB", "testFieldTypeBBA"]
                    }, false)                                                            
                })
                
                it("validates the type references in the fields", function(){
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["testFieldTypeAAA", "testFieldTypeAAB"], {
                        filename: "Test Filename C",
                        line: 556,
                        tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                    }, false)
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["testFieldTypeABA"], {
                        filename: "Test Filename D",
                        line: 357,
                        tokens: ["testFieldNameAB", "testFieldTypeABA"]
                    }, false)                    
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["?", "testFieldTypeBAB"], {
                        filename: "Test Filename H",
                        line: 2738,
                        tokens: ["testFieldNameBA", "?", "testFieldTypeBAB"]
                    }, true)                                        
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["testFieldTypeBBA"], {
                        filename: "Test Filename I",
                        line: 75825,
                        tokens: ["testFieldNameBB", "testFieldTypeBBA"]
                    }, true)                                                            
                })
                
                it("throws an exception when indentation follows a field declaration", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 9983,
                            tokens: ["nonGeneric", "combines"]
                        }, {
                            filename: "Test Filename B",
                            line: 887,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 556,
                            tokens: ["testFieldNameAA", "testFieldTypeAAA", "testFieldTypeAAB"]
                        }, {
                            filename: "Test Filename D",
                            line: 357,
                            indent: true
                        }])
                    }).toThrow({
                        message: "unexpectedIndentation",
                        line: {
                            filename: "Test Filename D",
                            line: 357,
                            indent: true
                        }                       
                    })                 
                })   
            })
            
            describe("functions", function(){
                var result 
                beforeEach(function(){
                    var input = [{
                        filename: "Test Filename A",
                        line: 23287,
                        tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                    }, {
                        filename: "Test Filename B",
                        line: 66989,
                        indent: true
                    }, {
                        filename: "Test Filename C",
                        line: 22897,
                        tokens: ["let", "letA", "content", "letAA"]
                    }, {
                        filename: "Test Let Filename AA",
                        line: 232387,
                        indent: true
                    }, {
                        filename: "Test Let Filename AB",
                        line: 76826,
                        tokens: ["letAA", "with", "chain"]
                    }, {
                        filename: "Test Let Filename AC",
                        line: 76826,
                        tokens: ["letAB"]
                    }, {
                        filename: "Test Let Filename AD",
                        line: 76826,
                        unindent: true
                    }, {
                        filename: "Test Filename D",
                        line: 2312968,
                        tokens: ["let", "letB", "alsoContent"]
                    }, {
                        filename: "Test Let Filename AE",
                        line: 35357,
                        indent: true
                    }, {
                        filename: "Test Let Filename AF",
                        line: 57368,
                        tokens: ["letBA", "with", "chain"]
                    }, {
                        filename: "Test Let Filename AG",
                        line: 5865368,
                        tokens: ["letBB"]
                    }, {
                        filename: "Test Let Filename AH",
                        line: 334367,
                        unindent: true
                    }, {
                        filename: "Test Filename E",
                        line: 7231,
                        tokens: ["fieldChainAA", "with", "functions"]
                    }, {
                        filename: "Test Filename F",
                        line: 31987,
                        tokens: ["fieldChainAB"]
                    }, {
                        filename: "Test Filename G",
                        line: 22768,
                        unindent: true
                    }, {
                        filename: "Test Filename H",
                        line: 2356,
                        tokens: ["generic", "takes", "?", "a", "type", "returns", "?", "anOutputType"]
                    }, {
                        filename: "Test Filename I",
                        line: 44987,
                        indent: true
                    }, {
                        filename: "Test Filename J",
                        line: 776734,
                        tokens: ["let", "letC", "content", "letBA"]
                    }, {
                        filename: "Test Let Filename BA",
                        line: 45346,
                        indent: true
                    }, {
                        filename: "Test Let Filename BB",
                        line: 245747,
                        tokens: ["letBA", "with", "chain"]
                    }, {
                        filename: "Test Let Filename BC",
                        line: 56573,
                        tokens: ["letBB"]
                    }, {
                        filename: "Test Let Filename BD",
                        line: 89783,
                        unindent: true
                    }, {
                        filename: "Test Filename K",
                        line: 11334,
                        tokens: ["let", "letD", "alsoContent"]
                    }, {
                        filename: "Test Let Filename BE",
                        line: 6346,
                        indent: true
                    }, {
                        filename: "Test Let Filename BF",
                        line: 245727,
                        tokens: ["letBC", "with", "chain"]
                    }, {
                        filename: "Test Let Filename BG",
                        line: 425728,
                        tokens: ["letBD"]
                    }, {
                        filename: "Test Let Filename BH",
                        line: 5685683,
                        unindent: true
                    }, {
                        filename: "Test Filename L",
                        line: 66752,
                        tokens: ["fieldChainBA", "with", "functions"]
                    }, {
                        filename: "Test Filename M",
                        line: 535,
                        tokens: ["fieldChainBB"]
                    }, {
                        filename: "Test Filename N",
                        line: 14134,
                        unindent: true
                    }]
                    
                    instance.validateAndCreateChain.and.callFake(function(tokens, line){
                        switch(line) {
                            case input[4]:
                                expect(tokens).toEqual(["letAA", "with", "chain"])
                                return "test let chain a a a"
                                
                            case input[5]:
                                expect(tokens).toEqual(["letAB"])
                                return "test let chain a a b"
                                
                            case input[9]:
                                expect(tokens).toEqual(["letBA", "with", "chain"])
                                return "test let chain a b a"
                                
                            case input[10]:
                                expect(tokens).toEqual(["letBB"])
                                return "test let chain a b b"
                                
                            case input[12]:
                                expect(tokens).toEqual(["fieldChainAA", "with", "functions"])
                                return "test field chain a a"                                
                                
                            case input[13]:
                                expect(tokens).toEqual(["fieldChainAB"])
                                return "test field chain a b"                                
                                
                            case input[19]:
                                expect(tokens).toEqual(["letBA", "with", "chain"])
                                return "test let chain b a a"
                                
                            case input[20]:
                                expect(tokens).toEqual(["letBB"])
                                return "test let chain b a b"
                                
                            case input[24]:
                                expect(tokens).toEqual(["letBC", "with", "chain"])
                                return "test let chain b b a"
                                
                            case input[25]:
                                expect(tokens).toEqual(["letBD"])
                                return "test let chain b b b"
                                
                            case input[27]:
                                expect(tokens).toEqual(["fieldChainBA", "with", "functions"])
                                return "test field chain b a"                                
                                
                            case input[28]:
                                expect(tokens).toEqual(["fieldChainBB"])
                                return "test field chain b b"                                                                
                        }
                    })
                    
                    result = instance.parse(input)
                })
                
                it("creates functions based on those in the file", function(){
                    expect(result.functions).toEqual([{
                        name: "nonGeneric",
                        line: {
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        },
                        takes: ["anInput", "type"],
                        returns: ["an", "output", "type"],
                        lets: {
                            letA: {
                                line: {
                                    filename: "Test Filename C",
                                    line: 22897,
                                    tokens: ["let", "letA", "content", "letAA"]
                                },
                                returns: ["content", "letAA"],
                                fields: ["test let chain a a a", "test let chain a a b"]
                            },
                            letB: {
                                line: {
                                    filename: "Test Filename D",
                                    line: 2312968,
                                    tokens: ["let", "letB", "alsoContent"]
                                },
                                returns: ["alsoContent"],
                                fields: ["test let chain a b a", "test let chain a b b"]
                            }
                        },
                        fields: ["test field chain a a", "test field chain a b"]
                    }, {
                        name: "generic",
                        line: {
                            filename: "Test Filename H",
                            line: 2356,
                            tokens: ["generic", "takes", "?", "a", "type", "returns", "?", "anOutputType"]
                        },
                        takes: ["?", "a", "type"],
                        returns: ["?", "anOutputType"],
                        lets: {
                            letC: {
                                line: {
                                    filename: "Test Filename J",
                                    line: 776734,
                                    tokens: ["let", "letC", "content", "letBA"]
                                },
                                returns: ["content", "letBA"],
                                fields: ["test let chain b a a", "test let chain b a b"]
                            },
                            letD: {
                                line: {
                                    filename: "Test Filename K",
                                    line: 11334,
                                    tokens: ["let", "letD", "alsoContent"]
                                },
                                returns: ["alsoContent"],
                                fields: ["test let chain b b a", "test let chain b b b"]
                            }
                        },
                        fields: ["test field chain b a", "test field chain b b"]
                    }])
                })
                
                it("validates their names", function(){
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["nonGeneric"], {
                        filename: "Test Filename A",
                        line: 23287,
                        tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                    }, false)
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["generic"], {
                        filename: "Test Filename H",
                        line: 2356,
                        tokens: ["generic", "takes", "?", "a", "type", "returns", "?", "anOutputType"]
                    }, false)                    
                })
                
                it("validates their input types", function(){
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["anInput", "type"], {
                        filename: "Test Filename A",
                        line: 23287,
                        tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                    }, true)
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["?", "a", "type"], {
                        filename: "Test Filename H",
                        line: 2356,
                        tokens: ["generic", "takes", "?", "a", "type", "returns", "?", "anOutputType"]
                    }, true)                    
                })
                
                it("validates their return types", function(){
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["an", "output", "type"], {
                        filename: "Test Filename A",
                        line: 23287,
                        tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                    }, false)
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["?", "anOutputType"], {
                        filename: "Test Filename H",
                        line: 2356,
                        tokens: ["generic", "takes", "?", "a", "type", "returns", "?", "anOutputType"]
                    }, true)                    
                })
                
                it("validates let names", function(){
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["letA"], {
                        filename: "Test Filename C",
                        line: 22897,
                        tokens: ["let", "letA", "content", "letAA"]
                    }, false)                    
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["letB"], {
                        filename: "Test Filename D",
                        line: 2312968,
                        tokens: ["let", "letB", "alsoContent"]
                    }, false)                    
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["letC"], {
                        filename: "Test Filename J",
                        line: 776734,
                        tokens: ["let", "letC", "content", "letBA"]
                    }, false)                    
                    
                    expect(instance.validateNameSyntax).toHaveBeenCalledWith(["letD"], {
                        filename: "Test Filename K",
                        line: 11334,
                        tokens: ["let", "letD", "alsoContent"]
                    }, false)                                        
                })
                
                it("validates let types", function(){
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["content", "letAA"], {
                        filename: "Test Filename C",
                        line: 22897,
                        tokens: ["let", "letA", "content", "letAA"]
                    }, false)                    
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["alsoContent"], {
                        filename: "Test Filename D",
                        line: 2312968,
                        tokens: ["let", "letB", "alsoContent"]
                    }, false)                    
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["content", "letBA"], {
                        filename: "Test Filename J",
                        line: 776734,
                        tokens: ["let", "letC", "content", "letBA"]
                    }, true)                    
                    
                    expect(instance.validateTypeReferenceSyntax).toHaveBeenCalledWith(["alsoContent"], {
                        filename: "Test Filename K",
                        line: 11334,
                        tokens: ["let", "letD", "alsoContent"]
                    }, true)                                                            
                })
                
                it("validates let chains", function(){
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letAA", "with", "chain"], {
                        filename: "Test Let Filename AB",
                        line: 76826,
                        tokens: ["letAA", "with", "chain"]
                    })                    
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letAB"], {
                        filename: "Test Let Filename AC",
                        line: 76826,
                        tokens: ["letAB"]
                    })                    
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letBA", "with", "chain"], {
                        filename: "Test Let Filename AF",
                        line: 57368,
                        tokens: ["letBA", "with", "chain"]
                    })                    
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letBB"], {
                        filename: "Test Let Filename AG",
                        line: 5865368,
                        tokens: ["letBB"]
                    })                    

                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letBA", "with", "chain"], {
                        filename: "Test Let Filename BB",
                        line: 245747,
                        tokens: ["letBA", "with", "chain"]
                    })                    
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letBB"], {
                        filename: "Test Let Filename BC",
                        line: 56573,
                        tokens: ["letBB"]
                    })                    
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letBC", "with", "chain"], {
                        filename: "Test Let Filename BF",
                        line: 245727,
                        tokens: ["letBC", "with", "chain"]
                    })                    
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["letBD"], {
                        filename: "Test Let Filename BG",
                        line: 425728,
                        tokens: ["letBD"]
                    })                                        
                })
                
                it("validates field chains", function(){
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["fieldChainAA", "with", "functions"], {
                        filename: "Test Filename E",
                        line: 7231,
                        tokens: ["fieldChainAA", "with", "functions"]
                    })                                        
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["fieldChainAB"], {
                        filename: "Test Filename F",
                        line: 31987,
                        tokens: ["fieldChainAB"]
                    })                                                            
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["fieldChainBA", "with", "functions"], {
                        filename: "Test Filename L",
                        line: 66752,
                        tokens: ["fieldChainBA", "with", "functions"]
                    })                                        
                    
                    expect(instance.validateAndCreateChain).toHaveBeenCalledWith(["fieldChainBB"], {
                        filename: "Test Filename M",
                        line: 535,
                        tokens: ["fieldChainBB"]
                    })                                                                                
                })
                
                it("throws an exception when lets are not uniquely named", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Let Filename AA",
                            line: 232387,
                            indent: true
                        }, {
                            filename: "Test Let Filename AB",
                            line: 76826,
                            tokens: ["letAA", "with", "chain"]
                        }, {
                            filename: "Test Let Filename AC",
                            line: 76826,
                            tokens: ["letAB"]
                        }, {
                            filename: "Test Let Filename AD",
                            line: 76826,
                            unindent: true
                        }, {
                            filename: "Test Filename D",
                            line: 2312968,
                            tokens: ["let", "letA", "alsoContent"]
                        }, {
                            filename: "Test Let Filename AE",
                            line: 35357,
                            indent: true
                        }, {
                            filename: "Test Let Filename AF",
                            line: 57368,
                            tokens: ["letBA", "with", "chain"]
                        }, {
                            filename: "Test Let Filename AG",
                            line: 5865368,
                            tokens: ["letBB"]
                        }, {
                            filename: "Test Let Filename AH",
                            line: 334367,
                            unindent: true
                        }, {
                            filename: "Test Filename E",
                            line: 7231,
                            tokens: ["fieldChainAA", "with", "functions"]
                        }, {
                            filename: "Test Filename F",
                            line: 31987,
                            tokens: ["fieldChainAB"]
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            unindent: true
                        }])
                    }).toThrow({
                        message: "letNamesNotUnique",
                        line: {
                            filename: "Test Filename D",
                            line: 2312968,
                            tokens: ["let", "letA", "alsoContent"]
                        }
                    })
                })
                
                it("throws an exception when a let defines no fields followed by another", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename D",
                            line: 2312968,
                            tokens: ["let", "letB", "alsoContent"]
                        },{
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Let Filename AF",
                            line: 57368,
                            tokens: ["letBA", "with", "chain"]
                        }, {
                            filename: "Test Let Filename AG",
                            line: 5865368,
                            tokens: ["letBB"]
                        }, {
                            filename: "Test Let Filename AH",
                            line: 334367,
                            unindent: true
                        }, {
                            filename: "Test Filename E",
                            line: 7231,
                            tokens: ["fieldChainAA", "with", "functions"]
                        }, {
                            filename: "Test Filename F",
                            line: 31987,
                            tokens: ["fieldChainAB"]
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            unindent: true
                        }])
                    }).toThrow({
                        message: "letEmpty",
                        line: {
                            filename: "Test Filename D",
                            line: 2312968,
                            tokens: ["let", "letB", "alsoContent"]
                        },
                    })
                })
                
                it("throws an exception when a let defines no fields followed by a field", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Filename E",
                            line: 7231,
                            tokens: ["fieldChainAA", "with", "functions"]
                        }, {
                            filename: "Test Filename F",
                            line: 31987,
                            tokens: ["fieldChainAB"]
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            unindent: true
                        }])
                    }).toThrow({
                        message: "letEmpty",
                        line: {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }
                    })
                })                
                
                it("throws an exception when a let defines no fields followed by a new declaration", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            unindent: true
                        }, {
                            filename: "Test Filename H",
                            line: 37,
                            tokens: ["x", "aliases", "y"]
                        }])
                    }).toThrow({
                        message: "letEmpty",
                        line: {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }
                    })
                })                

                it("throws an exception when a let defines no fields followed by the end of the file", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            unindent: true
                        }])
                    }).toThrow({
                        message: "letEmpty",
                        line: {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }
                    })
                })                
                                
                it("throws an exception when no fields are present at the end of the file", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }])
                    }).toThrow({
                        message: "functionEmpty",
                        line: {
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }
                    })
                })
                
                it("throws an exception when no fields are present when followed by a new declaration", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename H",
                            line: 37,
                            tokens: ["x", "aliases", "y"]
                        }])
                    }).toThrow({
                        message: "functionEmpty",
                        line: {
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }
                    })
                })                                
                
                it("throws an exception when no fields are present when followed by a new functiondeclaration", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 23524657,
                            tokens: ["nonGeneric2", "takes", "inputType", "returns", "outputType"]
                        }, {
                            filename: "Test Filename C",
                            line: 2352,
                            indent: true 
                        }, {
                            filename: "Test Filename D",
                            line: 43587,
                            tokens: ["2356.5"]
                        }, {
                            filename: "Test Filename E",
                            line: 9879,
                            unindent: true
                        }])
                    }).toThrow({
                        message: "functionEmpty",
                        line: {
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }
                    })
                })                                                
                
                it("throws an exception when no fields are present at the end of the file with lets", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Let Filename AA",
                            line: 232387,
                            indent: true
                        }, {
                            filename: "Test Let Filename AF",
                            line: 57368,
                            tokens: ["letBA", "with", "chain"]
                        }, {
                            filename: "Test Let Filename AG",
                            line: 5865368,
                            tokens: ["letBB"]
                        }, {
                            filename: "Test Let Filename AH",
                            line: 334367,
                            unindent: true
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            unindent: true
                        }])
                    }).toThrow({
                        message: "functionEmpty",
                        line: {
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }
                    })
                })
                
                it("throws an exception when no fields are present when followed by a new declaration with lets", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Let Filename AA",
                            line: 232387,
                            indent: true
                        }, {
                            filename: "Test Let Filename AF",
                            line: 57368,
                            tokens: ["letBA", "with", "chain"]
                        }, {
                            filename: "Test Let Filename AG",
                            line: 5865368,
                            tokens: ["letBB"]
                        }, {
                            filename: "Test Let Filename AH",
                            line: 334367,
                            unindent: true
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            unindent: true
                        }, {
                            filename: "Test Filename H",
                            line: 37,
                            tokens: ["x", "aliases", "y"]
                        }])
                    }).toThrow({
                        message: "functionEmpty",
                        line: {
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }
                    })
                })       
                
                it("throws an exception when indentation unexpectedly follows a let field", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename C",
                            line: 22897,
                            tokens: ["let", "letA", "content", "letAA"]
                        }, {
                            filename: "Test Let Filename AA",
                            line: 232387,
                            indent: true
                        }, {
                            filename: "Test Let Filename AB",
                            line: 76826,
                            tokens: ["letAA", "with", "chain"]
                        }, {
                            filename: "Test Let Filename AC",
                            line: 76826,
                            indent: true
                        }])
                    }).toThrow({
                        message: "unexpectedIndentation",
                        line: {
                            filename: "Test Let Filename AC",
                            line: 76826,
                            indent: true
                        }
                    })
                })                
                
                it("throws an exception when a field is unexpectedly followed by indentation", function(){
                    expect(function(){
                        instance.parse([{
                            filename: "Test Filename A",
                            line: 23287,
                            tokens: ["nonGeneric", "takes", "anInput", "type", "returns", "an", "output", "type"]
                        }, {
                            filename: "Test Filename B",
                            line: 66989,
                            indent: true
                        }, {
                            filename: "Test Filename F",
                            line: 31987,
                            tokens: ["fieldChainAB"]
                        }, {
                            filename: "Test Filename G",
                            line: 22768,
                            indent: true
                        }])
                    }).toThrow({
                        message: "unexpectedIndentation",
                        line: {
                            filename: "Test Filename G",
                            line: 22768,
                            indent: true
                        }
                    })                    
                })
                
                // We don't throw exceptions for functions not being uniquely 
                // named because we need to check the input type, not just the
                // name.  This will be done in the uniqueness check pass later.
            })
        })
    })
})