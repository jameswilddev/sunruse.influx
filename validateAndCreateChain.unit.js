describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("validateAndCreateChain", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
                instance.primitives = {
                    primitiveA: {
                        regex: /^valueFromPrimitiveA$/
                    },
                    primitiveB: {
                        regex: /^valueFromPrimitiveB$/
                    },
                    primitiveC: {
                        regex: /^valueFromPrimitiveC$/
                    }                   
                }                  
            })
            
            it("throws an exception when empty", function(){
                expect(function(){
                    instance.validateAndCreateChain([], "Test Line")
                }).toThrow({
                    line: "Test Line",
                    message: "chainEmpty"
                })
            })
            
            it("throws an exception when starting from 'let'", function(){
                expect(function(){
                    instance.validateAndCreateChain(["let", "something"], "Test Line")
                }).toThrow({
                    line: "Test Line",
                    message: "valueExpectedKeywordGiven"
                })
            })
            
            it("throws an exception when starting from 'returns'", function(){
                expect(function(){
                    instance.validateAndCreateChain(["returns", "something"], "Test Line")
                }).toThrow({
                    line: "Test Line",
                    message: "valueExpectedKeywordGiven"
                })
            })            
            
            it("throws an exception when starting from 'takes'", function(){
                expect(function(){
                    instance.validateAndCreateChain(["takes", "something"], "Test Line")
                }).toThrow({
                    line: "Test Line",
                    message: "valueExpectedKeywordGiven"
                })
            })                        
            
            it("throws an exception when starting from 'combines'", function(){
                expect(function(){
                    instance.validateAndCreateChain(["combines", "something"], "Test Line")
                }).toThrow({
                    line: "Test Line",
                    message: "valueExpectedKeywordGiven"
                })
            })                        
            
            var shared = function(start, checkResult){
                describe("when no chain follows", function(){
                    var result, result2 = []
                    beforeEach(function(){
                        result2[0] = result = instance.validateAndCreateChain([start], "Test Line")
                    })
                    
                    it("copies the line", function(){
                        expect(result.line).toEqual("Test Line")
                    })
                    
                    it("creates an empty array of function names", function(){
                        expect(result.functions).toEqual([])
                    })
                    
                    checkResult(result2)
                })
                
                describe("when a chain follows", function(){
                    var result, result2 = []
                    beforeEach(function(){
                        result2[0] = result = instance.validateAndCreateChain([start, "test", "functions"], "Test Line")
                    })
                    
                    it("copies the line", function(){
                        expect(result.line).toEqual("Test Line")
                    })
                    
                    it("copies the function chain", function(){
                        expect(result.functions).toEqual(["test", "functions"])
                    })
                    
                    checkResult(result2)
                })

                it("throws an exception when the chain contains 'input'", function(){
                    expect(function(){
                        instance.validateAndCreateChain([start, "something", "input", "else"], "Test Line")
                    }).toThrow({
                        line: "Test Line",
                        message: "functionExpectedKeywordGiven"
                    })                    
                })
                
                it("throws an exception when the chain contains 'let'", function(){
                    expect(function(){
                        instance.validateAndCreateChain([start, "something", "let", "else"], "Test Line")
                    }).toThrow({
                        line: "Test Line",
                        message: "functionExpectedKeywordGiven"
                    })                    
                })
                
                it("throws an exception when the chain contains 'takes'", function(){
                    expect(function(){
                        instance.validateAndCreateChain([start, "something", "takes", "else"], "Test Line")
                    }).toThrow({
                        line: "Test Line",
                        message: "functionExpectedKeywordGiven"
                    })                    
                })                    
                
                it("throws an exception when the chain contains 'returns'", function(){
                    expect(function(){
                        instance.validateAndCreateChain([start, "something", "returns", "else"], "Test Line")
                    }).toThrow({
                        line: "Test Line",
                        message: "functionExpectedKeywordGiven"
                    })                                        
                })                    
                
                it("throws an exception when the chain contains 'combines'", function(){
                    expect(function(){
                        instance.validateAndCreateChain([start, "something", "combines", "else"], "Test Line")
                    }).toThrow({
                        line: "Test Line",
                        message: "functionExpectedKeywordGiven"
                    })                    
                })                    
                
                it("throws an exception when the chain contains primitive literals", function(){
                    expect(function(){
                        instance.validateAndCreateChain([start, "something", "valueFromPrimitiveB", "else"], "Test Line")
                    }).toThrow({
                        line: "Test Line",
                        message: "functionExpectedPrimitiveLiteralGiven"
                    })                    
                })                    
                
                it("throws an exception when the chain contains primitive types", function(){
                    expect(function(){
                        instance.validateAndCreateChain([start, "something", "primitiveB", "else"], "Test Line")
                    }).toThrow({
                        line: "Test Line",
                        message: "functionExpectedPrimitiveGiven"
                    })  
                })                    
            }
            
            describe("when starting from a literal", function(){
                shared("valueFromPrimitiveB", function(result){
                    it("sets literal to the literal", function(){
                        expect(result[0].primitive).toEqual("valueFromPrimitiveB")
                    })
                    
                    it("sets input to falsy", function(){
                        expect(result[0].input).toBeFalsy()
                    })
                    
                    it("sets let to falsy", function(){
                        expect(result[0].let).toBeFalsy()
                    })                    
                })
            })
            
            describe("when starting from 'input'", function(){
                shared("input", function(result){
                    it("sets literal to falsy", function(){
                        expect(result[0].literal).toBeFalsy()
                    })
                    
                    it("sets input to true", function(){
                        expect(result[0].input).toBeTruthy()
                    })
                    
                    it("sets let to falsy", function(){
                        expect(result[0].let).toBeFalsy()
                    })                    
                })
            })
            
            describe("when starting from a let", function(){
                shared("misc", function(result){
                    it("sets literal to falsy", function(){
                        expect(result[0].literal).toBeFalsy()
                    })
                    
                    it("sets input to falsy", function(){
                        expect(result[0].input).toBeFalsy()
                    })
                    
                    it("sets let to the let", function(){
                        expect(result[0].let).toEqual("misc")
                    })                    
                })
            })            
        })
    })
})