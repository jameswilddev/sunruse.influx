ide = {
    project: {
        name: "untitled project",
        created: new Date().toISOString(),
        files: {},
        history: {
            steps: [],
            undoneTo: 0
        }
    },
    stepTypes: {
    },
    undo: function(elements, json) {
        var step = json.steps[--json.undoneTo]
        ide.stepTypes[step.type].undo(step)
        ide.refreshHistory(elements, json)
    },
    redo: function(elements, json) {
        var step = json.steps[json.undoneTo++]
        ide.stepTypes[step.type].redo(step)
        ide.refreshHistory(elements, json)
    },
    addHistory: function(elements, json, step) {
        step.created = new Date().toISOString()
        json.steps.splice(json.undoneTo)
        json.steps.push(step)
        ide.redo(elements, json)
    },
    refreshHistory: function(elements, json){
        if(!json.steps.length) {
            elements.find(".undo, .redo").addClass("disabled")
        } else {
            if(json.undoneTo == json.steps.length) {
                elements.find(".undo").removeClass("disabled")
                elements.find(".redo").addClass("disabled")
            } else {
                if(json.undoneTo == 0)
                    elements.find(".undo").addClass("disabled")
                else
                    elements.find(".undo").removeClass("disabled")
                elements.find(".redo").removeClass("disabled")
            }
        }
    }
}

$(document).ready(function(){
    ide.refreshHistory($("#menu"), ide.project.history)

    $("body").click(function(e){
        var target = $(e.target)
        
        if(target.is("#menu > .undo:not(.disabled)")) ide.undo($("#menu"), ide.project.history)
        if(target.is("#menu > .redo:not(.disabled)")) ide.redo($("#menu"), ide.project.history)
    })

    $("#menu > .import").change(function(){
        var f = new FileReader()
        f.onload = function(){
            ide.project = JSON.parse(f.result)
            fileBrowser.refresh($(".file-browser"))
        }
        var files = $("#menu > .import > input").prop("files")
        if(!files || !files[0]) return
        f.readAsText(files[0])
    })
    
    $("#menu > .export").click(function(){
        var blob = new Blob([JSON.stringify(ide.project)], {type: "application/json;charset=utf-8"});
        saveAs(blob, ide.project.name + ".json");        
    })
})