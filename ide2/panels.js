$(document).ready(function(){
    var updateTabLabel = function(content){
        var tab = content.data("tab")
        if(!tab) return
        tab.data("content", content)
        tab.children(".label").text(content.children("h2").text())
    }                

    $("#root").load("newtab.html")
    
    var started
    
    // Get the position of the pointer as a percentage from left 0% to right 100% within a target element.
    var positionX = function(e, target){
        return ((e.pageX - target.position().left) / target.width()) * 100
    }
    
    // Get the position of the pointer as a percentage from top 0% to bottom 100% within a target element.
    var positionY = function(e, target){
        return ((e.pageY - target.position().top) / target.height()) * 100
    }
    
    $("body").on("touchstart mousedown", function(e){
        var target = $(e.target)
        
        if(target.is("h2 > .label")){
            target.addClass("dragging")
        }
        
        if(target.is(".grab-horizontal")) {
            started = positionX(e, target.closest(".panel"))
            target.addClass("dragging")
        }
        
        if(target.is(".grab-vertical")) {
            started = positionY(e, target.closest(".panel"))
            target.addClass("dragging")
        }
    })
    
    $("body").on("touchmove mousemove", function(e){
        var dragging = $(".dragging")
        var draggingPanel = dragging.closest(".panel")
        var target = $(e.target)
        $(".drag-target").removeClass("drag-target")
        if(dragging.is(".grab-horizontal")) {
            var left = draggingPanel.children(".left")
            var right = draggingPanel.children(".right")
            var pos = positionX(e, draggingPanel)
            var pos2 = Math.min(90, Math.max(10, (parseFloat(right.css("left")) * 100 / draggingPanel.width()) + pos - started))
            left.css({"right": 100 - pos2 + "%"})
            right.css({"left": pos2 + "%"})
            dragging.parent().css({"right": 100 - pos2 + "%"})
            started = pos
        }
        if(dragging.is(".grab-vertical")) {
            var top = draggingPanel.children(".top")
            var bottom = draggingPanel.children(".bottom")
            var pos = positionY(e, draggingPanel)
            var pos2 = Math.min(90, Math.max(10, (parseFloat(bottom.css("top")) * 100 / draggingPanel.height()) + pos - started))
            top.css({"bottom": 100 - pos2 + "%"})
            bottom.css({"top": pos2 + "%"})
            dragging.parent().css({"bottom": 100 - pos2 + "%"})
            started = pos
        }
        if(dragging.is("h2 > .label") && target.is("h2 > .label"))
            target.parent().parent().addClass("drag-target")
    })
    
    $("body").on("touchend mouseup", function(e){
        var target = $(e.target)
        var dragging = $(".dragging")
        
        if(dragging.is("h2 > .label") && target.is("h2 > .label")){
            var from = dragging.parent().parent().clone()
            var to = target.parent().parent().clone()
            
            to.attr("class", dragging.parent().parent().attr("class") || "")
            from.attr("class", target.parent().parent().attr("class") || "")
            to.attr("style", dragging.parent().parent().attr("style") || "")
            from.attr("style", target.parent().parent().attr("style") || "")
            to.data("tab", dragging.parent().parent().data("tab"))
            from.data("tab", target.parent().parent().data("tab"))      
            to.attr("id", dragging.parent().parent().attr("id") || "")
            from.attr("id", target.parent().parent().attr("id") || "")
            
            dragging.parent().parent().replaceWith(to)
            target.parent().parent().replaceWith(from)
            
            updateTabLabel(to)
            updateTabLabel(from)
        }
        
        $(".drag-target").removeClass("drag-target")
        $(".dragging").removeClass("dragging")
    })
    
    $("body").click(function(e){
        var target = $(e.target)
        
        var doOption = function(type, name) {
            if(target.is("." + type + " > ." + name)) target.parent().parent().parent().fadeOut(150, function(){
                var classes = $(this).attr("class") || ""
                var tab = $(this).data("tab")
                $(this).load(name + ".html", function() {
                    $(this).attr("class", classes).fadeIn(150).data("tab", tab)
                    updateTabLabel($(this))
                })
            })
        }
        
        doOption("layout", "tabs")
        doOption("layout", "horizontal-split")
        doOption("layout", "vertical-split")
        doOption("views", "browser")
        doOption("views", "assertions")
        doOption("views", "generated-code")
        doOption("views", "new-file")
        
        if(target.is("h2 > .wrap")) {
            var content = target.parent().parent()
            var old = content.clone()
            content.fadeOut(150, function(){
                content.load("horizontal-split.html", function(){
                    var replacing = content.children(".left")
                    old.attr("class", replacing.attr("class") || "")
                    old.attr("style", replacing.attr("style") || "")
                    old.attr("id", replacing.attr("id") || "")
                    replacing.replaceWith(old)
                    updateTabLabel(content)
                    content.fadeIn(150)
                })
            })
        }                    
        
        if(target.is(".tab-bar > .tab > .wrap")) {
            var content = target.parent().data("content")
            var old = content.clone()
            content.fadeOut(150, function(){
                content.load("horizontal-split.html", function(){
                    var replacing = content.children(".left")
                    old.attr("class", replacing.attr("class") || "")
                    old.attr("style", replacing.attr("style") || "")
                    old.attr("id", replacing.attr("id") || "")
                    replacing.replaceWith(old)
                    updateTabLabel(content)
                    content.fadeIn(150)
                })
            })
        }

        if(target.is(".tab-bar > .tab > .label")) {
            var tab = target.parent()
            tab.siblings().removeClass("active")
            tab.addClass("active")
            var contents = tab.parent().parent().children(".tab-content").children()
            contents.fadeOut(150)
            tab.data("content").fadeIn(150)
        }
        
        if(target.is(".tab-bar > .tab > .close")) {
            var tab = target.parent()
            var sibs = tab.siblings(".tab")
            var first = sibs.first()
            tab.data("content").fadeOut(150, function(){
                $(this).remove()
            })
            tab.fadeOut(150, function(){
                $(this).remove()
            })
            
            if(!sibs.is(".active")) sibs.first().children(".label").click()
        }
        
        if(target.is(".tab-bar > .add")) {
            target.siblings().removeClass("active")
            var tab = $("<span class='tab active'><span class='label'>new tab</span><span class='fa fa-columns wrap clickable'></span><span class='fa fa-times close clickable'></span></span>")
            var content = $("<div class='tab panel'></div>").data("tab", tab)
            tab.hide().insertBefore(target).fadeIn().data("content", content)
            
            target.parent().parent().children(".tab-content").children().fadeOut(150)
            target.parent().parent().children(".tab-content").append(content.hide().load("newtab.html", function(){
                $(this).fadeIn(150)
            }))
        }
        
        var doMerge = function(direction, opposite) {
            if(target.is(".merge-" + direction)) {
                target = target.parent()
                target.parent().fadeOut(150, function(){
                    var sib = $(target.siblings("." + opposite).first())
                    sib.removeClass()
                    sib.attr("class", target.parent().attr("class") || "")
                    sib.attr("style", target.parent().attr("style") || "")
                    sib.data("tab", target.parent().data("tab"))
                    sib.attr("id", target.parent().attr("id") || "")
                    target.parent().replaceWith(sib)
                    updateTabLabel(sib)
                    sib.fadeIn()
                })
            }
        }
        
        doMerge("left", "right")
        doMerge("right", "left")
        doMerge("up", "bottom")
        doMerge("down", "top")
        
        if(target.is(".left ~ .split-bar > .turn")) {
            var left = $(target.parent().siblings(".left").first())
            var right = $(target.parent().siblings(".right").first())
            var split = left.width() * 100 / left.parent().width()
            target.parent().siblings("h2").children(".label").text("vertical split")
            updateTabLabel(target.parent().parent())
            left.removeClass("left").addClass("top").css({bottom: 100 - split + "%", right: ""})
            right.removeClass("right").addClass("bottom").css({top: split + "%", left: ""})
            target.parent().css({ bottom: 100 - split + "%", right: "" }).load("vertical-split-bar.html")
        } else if(target.is(".top ~ .split-bar > .turn")) {
            var top = $(target.parent().siblings(".top").first())
            var bottom = $(target.parent().siblings(".bottom").first())
            var split = bottom.height() * 100 / top.parent().height()
            target.parent().siblings("h2").children(".label").text("horizontal split")
            updateTabLabel(target.parent().parent())
            bottom.removeClass("bottom").addClass("left").css({right: 100 - split + "%", top: ""})
            top.removeClass("top").addClass("right").css({left: split + "%", bottom: ""})
            target.parent().css( { right: 100 - split + "%", bottom: "" }).load("horizontal-split-bar.html")
        }
    })
})