fileBrowser = {
    addRow: function(browsers, name) {
        browsers.find(".file-list").append($("<tr>").append($("<td class='clickable'>").text(name)).append($("<td class='clickable fa fa-pencil'>")).data("filename", name))
    },
    refresh: function(browsers) {
        browsers.find(".file-list").empty()
        for(var filename in ide.project.files)
            fileBrowser.addRow(browsers, filename)
    }
}

ide.stepTypes.addFile = {
    undo: function(step){
        delete ide.project.files[step.filename]
        fileBrowser.refresh($(".file-browser"))
    },
    redo: function(step){
        ide.project.files[step.filename] = {
            source: "",
            created: step.created
        }
        
        fileBrowser.addRow($(".file-browser"), step.filename)
    }            
}

$(document).ready(function(){
    $("body").click(function(e){
        var target = $(e.target)
        if(target.is(".file-browser > .add")) {
            var num = 1
            var filename
            do
                filename = "Untitled File " + num++
            while(ide.project.files[filename])
            
            ide.addHistory($("#menu"), ide.project.history, {
                type: "addFile",
                filename: filename
            })
        }
    })
})