describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("validateTypeReferenceSyntax", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "getPrimitiveTypeNameByLiteral")
                instance.primitives = {
                    primitiveA: {
                        regex: /^valueFromPrimitiveA$/
                    },
                    primitiveB: {
                        regex: /^valueFromPrimitiveB$/
                    },
                    primitiveC: {
                        regex: /^valueFromPrimitiveC$/
                    }                   
                }                  
            })
            
            var sharedTests = function(allowGenerics) {
                it("throws an exception if the combination chain contains a generic", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["base", "combination", "?", "more"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "onlyTheBaseTypeMayBeGeneric"
                    })
                })
                
                it("checks if any of the tokens are primitive literals", function(){
                    instance.validateTypeReferenceSyntax(["base", "combination", "more"], "Test Line", allowGenerics)
                    expect(instance.getPrimitiveTypeNameByLiteral).toHaveBeenCalledWith("base")
                    expect(instance.getPrimitiveTypeNameByLiteral).toHaveBeenCalledWith("combination")
                    expect(instance.getPrimitiveTypeNameByLiteral).toHaveBeenCalledWith("more")
                })
                
                it("throws an exception if any of the tokens are primitive literals", function(){
                    instance.getPrimitiveTypeNameByLiteral.and.callFake(function(token){
                        return token == "combination"
                    })
                    
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["base", "combination", "more"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "typeReferencesMayNotContainPrimitiveLiterals"
                    })
                })
                
                it("throws an exception if the type reference is empty", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax([], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "typeReferenceEmpty"
                    })
                })
                
                it("allows single token type declarations", function(){
                    instance.validateTypeReferenceSyntax(["test"], "Test Line", allowGenerics)
                })
                
                it("allows single token type declarations of primitive types", function(){
                    instance.validateTypeReferenceSyntax(["primitiveB"], "Test Line", allowGenerics)
                })
                
                it("allows building on primitive types", function(){
                    instance.validateTypeReferenceSyntax(["primitiveB", "test"], "Test Line", allowGenerics)
                })

                it("throws an exception if the type reference contains the input keyword", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["test", "input", "type"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "typeReferencesMayNotContainKeywords"
                    })
                })
                
                it("throws an exception if the type reference contains the let keyword", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["test", "let", "type"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "typeReferencesMayNotContainKeywords"
                    })
                })
                
                it("throws an exception if the type reference contains the combines keyword", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["test", "combines", "type"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "typeReferencesMayNotContainKeywords"
                    })
                })
                
                it("throws an exception if the type reference contains the takes keyword", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["test", "takes", "type"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "typeReferencesMayNotContainKeywords"
                    })
                })
                
                it("throws an exception if the type reference contains the returns keyword", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["test", "returns", "type"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "typeReferencesMayNotContainKeywords"
                    })
                })
                
                it("throws an exception if primitive types are used as generic combinations", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["test", "primitiveB"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "primitiveTypesMayNotBeUsedAsGenericCombinations"
                    })
                })
            }
            
            describe("when allowing generics", function(){
                it("allows building on generic types", function(){
                    instance.validateTypeReferenceSyntax(["?", "test"], "Test Line", true)
                })
                
                sharedTests(true)
            })
            
            describe("when not allowing generics", function(){
                it("throws an exception if the first token is a generic", function(){
                    expect(function(){
                        instance.validateTypeReferenceSyntax(["?", "combination", "more"], "Test Line", false)
                    }).toThrow({
                        line: "Test Line",
                        message: "genericTypeUsedInNonGenericContext"
                    })
                })
                
                sharedTests(false)
            })
        })
    })
})