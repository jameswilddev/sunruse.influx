describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("typeReferencesMatch", function(){
            it("?, ? returns ?", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?"], ["?"])).toEqual(["?"])
            })
            
            it("int, ? returns int", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["int"], ["?"])).toEqual(["int"])
            })            
            
            it("?, int returns int", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?"], ["int"])).toEqual(["int"])
            })            
            
            it("int, int returns empty", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["int"], ["int"])).toEqual([])
            })            
            
            it("float, int returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["float"], ["int"])).toBeFalsy()
            })            
            
            it("? combo, ? returns ? combo", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?", "combo"], ["?"])).toEqual(["?", "combo"])
            })            
            
            it("?, ? combo returns ? combo", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?"], ["?", "combo"])).toEqual(["?", "combo"])
            })                        
            
            it("? combo, combo returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?", "combo"], ["combo"])).toBeFalsy()
            })            
            
            it("combo, ? combo returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["combo"], ["?", "combo"])).toBeFalsy()
            })                                    
            
            it("int combo, ? returns int combo", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["int", "combo"], ["?"])).toEqual(["int", "combo"])
            })                        
            
            it("?, int combo returns int combo", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?"], ["int", "combo"])).toEqual(["int", "combo"])
            })                                    
            
            it("int combo, ? combo returns int", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["int", "combo"], ["?", "combo"])).toEqual(["int"])
            })                        
            
            it("? combo, int combo returns int", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?", "combo"], ["int", "combo"])).toEqual(["int"])
            })                                                
            
            it("int combo, float combo returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["int", "combo"], ["float", "combo"])).toBeFalsy()
            })                        
            
            it("float combo, int combo returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["float", "combo"], ["int", "combo"])).toBeFalsy()
            })                                                            
            
            it("int combo bigger, ? combo returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["int", "combo", "bigger"], ["?", "combo"])).toBeFalsy()
            })                        
            
            it("? combo, int combo bigger returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?", "combo"], ["int", "combo", "bigger"])).toBeFalsy()
            })                                                            
            
            it("? combo, int combo bigger returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["int", "combo", "bigger"], ["?", "combo"])).toBeFalsy()
            })                        
            
            it("int combo bigger, ? combo returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?", "combo"], ["int", "combo", "bigger"])).toBeFalsy()
            })                                                                        
            
            it("? one, ? two returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["?", "one"], ["?", "two"])).toBeFalsy()
            })                                                                                    
            
            it("combo one, combo two returns falsy", function(){
                expect(SUNRUSE.influx().typeReferencesMatch(["combo", "one"], ["combo", "two"])).toBeFalsy()
            })                                                                                                
        })
    })
})