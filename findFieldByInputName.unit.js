describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("findFieldByInputAndName", function(){
            var findFor, parsed
            beforeEach(function(){
                findFor = function(type, name){
                    return SUNRUSE.influx().findFieldByInputAndName(type, name, parsed = {
                        combinations: {
                            clash: {
                                fields: {
                                    clashA: {
                                        of: ["nonGenericClashA"]
                                    },
                                    clashB: {
                                        of: ["nonGeneric", "clashB"]
                                    }
                                }
                            },
                            nonGeneric: {
                                fields: {
                                    nonGenericA: {
                                        of: ["nongeneric", "field", "a"]
                                    },
                                    nonGenericB: {
                                        of: ["nongeneric", "b"]
                                    }
                                }
                            }
                        },
                        genericCombinations: {
                            clash: {
                                fields: {
                                    clashA: {
                                        of: ["?", "generic", "clash", "a"]
                                    }, 
                                    clashB: {
                                        of: ["generic", "clashB"]
                                    }
                                }
                            },
                            generic: {
                                fields: {
                                    genericA: {
                                        of: ["nonextension", "field"]
                                    },
                                    genericB: {
                                        of: ["?", "extension", "field"]
                                    }
                                }
                            }
                        }                        
                    })
                }
            })
            
            it("can retrieve fields from generic combinations", function(){
                expect(findFor(["?", "generic"], "genericA")).toEqual({
                    of: ["nonextension", "field"],
                    field: parsed.genericCombinations.generic.fields.genericA
                })
                expect(findFor(["base", "generic"], "genericA")).toEqual({
                    of: ["nonextension", "field"],
                    field: parsed.genericCombinations.generic.fields.genericA
                })
                expect(findFor(["?", "indirect", "generic"], "genericA")).toEqual({
                    of: ["nonextension", "field"],
                    field: parsed.genericCombinations.generic.fields.genericA
                })                
                
                expect(findFor(["?", "generic"], "genericB")).toEqual({
                    of: ["?", "extension", "field"],
                    field: parsed.genericCombinations.generic.fields.genericB
                })
                expect(findFor(["base", "generic"], "genericB")).toEqual({
                    of: ["base", "extension", "field"],
                    field: parsed.genericCombinations.generic.fields.genericB
                })
                expect(findFor(["?", "indirect", "generic"], "genericB")).toEqual({
                    of: ["?", "indirect", "extension", "field"],
                    field: parsed.genericCombinations.generic.fields.genericB
                })
            })
            
            it("can retrieve fields from non-generic combinations", function(){
                expect(findFor(["nonGeneric"], "nonGenericB")).toEqual({
                    of: ["nongeneric", "b"],
                    field: parsed.combinations.nonGeneric.fields.nonGenericB
                })
            })            
            
            it("returns falsy when a generic combination exists but no matching field", function(){
                expect(findFor(["?", "generic"], "nonGenericA")).toBeFalsy()
                expect(findFor(["base", "generic"], "nonGenericA")).toBeFalsy()
                expect(findFor(["?", "indirect", "generic"], "nonGenericA")).toBeFalsy()
            })         
            
            it("returns falsy when a nongeneric combination exists but no matching field", function(){
                expect(findFor(["nonGeneric"], "genericB")).toBeFalsy()
            })              
            
            it("returns falsy when \"?\" is given as a type", function(){
                expect(findFor(["?"], "nonGenericA")).toBeFalsy()
                expect(findFor(["?"], "genericB")).toBeFalsy()  
            })
            
            it("returns falsy when primitive types are given", function(){
                expect(findFor(["float"], "nonGenericA")).toBeFalsy()
                expect(findFor(["float"], "genericB")).toBeFalsy()  
            })
            
            it("does not confuse generic and nongeneric combinations of the same name", function(){
                expect(findFor(["?", "clash"], "clashA")).toEqual({
                    field: parsed.genericCombinations.clash.fields.clashA,
                    of: ["?", "generic", "clash", "a"]
                })                
                expect(findFor(["base", "clash"], "clashA")).toEqual({
                    field: parsed.genericCombinations.clash.fields.clashA,
                    of: ["base", "generic", "clash", "a"]
                })                  
                expect(findFor(["?", "clash"], "clashB")).toEqual({
                    field: parsed.genericCombinations.clash.fields.clashB,
                    of: ["generic", "clashB"]
                })
                expect(findFor(["base", "clash"], "clashB")).toEqual({
                    field: parsed.genericCombinations.clash.fields.clashB,
                    of: ["generic", "clashB"]
                })          
                
                expect(findFor(["clash"], "clashA")).toEqual({
                    field: parsed.combinations.clash.fields.clashA,
                    of: ["nonGenericClashA"]
                })    
                expect(findFor(["clash"], "clashB")).toEqual({
                    field: parsed.combinations.clash.fields.clashB,
                    of: ["nonGeneric", "clashB"]
                })                
            })
        })
    })
})