describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("checkForAliasLoops", function(){
            it("does nothing when there are no aliases", function(){
                SUNRUSE.influx().checkForAliasLoops({
                    aliases: {},
                    genericAliases: {}
                })
            })
            
            it("does nothing when no aliases recurse", function(){
                SUNRUSE.influx().checkForAliasLoops({
                    aliases: {
                        notRecursive: {
                            of: ["totally", "different", "types"]
                        }
                    },
                    genericAliases: {
                        nonRecursive: {
                            of: ["?", "completely", "different", "types"]
                        }
                    }
                })
            })
            
            it("allows base types to recurse", function(){
                SUNRUSE.influx().checkForAliasLoops({
                    aliases: {
                        parent: {
                            of: ["child", "different", "types"]
                        },
                        child: {
                            of: ["totally", "legal", "recursion"]
                        },
                        totally: {
                            of: ["utterly", "completely"]
                        }
                    },
                    genericAliases: {
                        nonRecursive: {
                            of: ["?", "completely", "different", "types"]
                        }
                    }
                })
            })
            
            it("allows base types to recurse with chains", function(){
                SUNRUSE.influx().checkForAliasLoops({
                    aliases: {
                        parent: {
                            of: ["child", "different", "types"]
                        },
                        child: {
                            of: ["totally", "legal", "aRecursion"]
                        },
                        totally: {
                            of: ["utterly", "completely"]
                        }
                    },
                    genericAliases: {
                        aRecursion: {
                            of: ["?", "completely", "different", "types"]
                        }
                    }
                })
            })
            
            it("allows chains to recurse", function(){
                SUNRUSE.influx().checkForAliasLoops({
                    aliases: {
                        notRecursive: {
                            of: ["totally", "different", "types"]
                        }
                    },
                    genericAliases: {
                        a: {
                            of: ["?", "completely", "b", "types"]
                        },
                        b: {
                            of: ["?", "many", "new", "types"]
                        }
                    }
                })
            })
            
            it("throws an exception should a single-base-type-alias loop exist", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            infiniteLoop: {
                                line: "Test Line A",
                                of: ["infiniteLoop"]
                            }
                        },
                        genericAliases: {
                            nonRecursive: {
                                of: ["?", "completely", "different", "types"]
                            }
                        }
                    })
                }).toThrow({
                    line: "Test Line A",
                    message: "infiniteLoopInNonGenericAlias"
                })
            })
            
            it("throws an exception should a single-base-type-alias loop exist with a chain", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            infiniteLoop: {
                                line: "Test Line A",
                                of: ["infiniteLoop", "with", "chain"]
                            }
                        },
                        genericAliases: {
                            nonRecursive: {
                                of: ["?", "completely", "different", "types"]
                            }
                        }
                    })
                }).toThrow({
                    line: "Test Line A",
                    message: "infiniteLoopInNonGenericAlias"
                })
            })            
            
            it("throws an exception should a two-base-type-alias loop exist", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            infinite: {
                                line: "Test Line A",
                                of: ["loop"]
                            },
                            loop: {
                                line: "Test Line A",
                                of: ["infinite"]
                            }                            
                        },
                        genericAliases: {
                            nonRecursive: {
                                of: ["?", "completely", "different", "types"]
                            }
                        }
                    })
                }).toThrow({
                    line: "Test Line A",
                    message: "infiniteLoopInNonGenericAlias"
                })
            })
            
            it("throws an exception should a two-base-type-alias loop exist with a chain", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            infinite: {
                                line: "Test Line A",
                                of: ["loop", "with", "chain"]
                            },
                            loop: {
                                line: "Test Line A",
                                of: ["infinite"]
                            }                            
                        },
                        genericAliases: {
                            nonRecursive: {
                                of: ["?", "completely", "different", "types"]
                            }
                        }
                    })
                }).toThrow({
                    line: "Test Line A",
                    message: "infiniteLoopInNonGenericAlias"
                })
            })                        
            
            it("throws an exception should a three-base-type-alias loop exist", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            infinite: {
                                line: "Test Line A",
                                of: ["spin"]
                            },
                            spin: {
                                line: "Test Line A",
                                of: ["loop"]
                            },
                            loop: {
                                line: "Test Line A",
                                of: ["infinite"]
                            }                            
                        },
                        genericAliases: {
                            nonRecursive: {
                                of: ["?", "completely", "different", "types"]
                            }
                        }
                    })
                }).toThrow({
                    line: "Test Line A",
                    message: "infiniteLoopInNonGenericAlias"
                })
            })
            
            it("throws an exception should a three-base-type-alias loop exist with a chain", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            infinite: {
                                line: "Test Line A",
                                of: ["spin"]
                            },
                            spin: {
                                line: "Test Line A",
                                of: ["loop", "with", "chain"]
                            },
                            loop: {
                                line: "Test Line A",
                                of: ["infinite"]
                            }                            
                        },
                        genericAliases: {
                            nonRecursive: {
                                of: ["?", "completely", "different", "types"]
                            }
                        }
                    })
                }).toThrow({
                    line: "Test Line A",
                    message: "infiniteLoopInNonGenericAlias"
                })
            })                        
            
            it("throws an exception should a single-chain-alias loop exist", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            notRecursive: {
                                of: ["totally", "different", "types"]
                            }
                        },
                        genericAliases: {
                            recursive: {
                                of: ["?", "completely", "recursive", "types"],
                                line: "Test Line B"
                            }
                        }
                    })
                }).toThrow({
                    message: "infiniteLoopInGenericAlias",
                    line: "Test Line B"
                })
            })
            
            it("throws an exception should a single-chain-alias loop exist ignoring its presence in base type aliases", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            notRecursive: {
                                of: ["totally", "recursive", "types"]
                            }
                        },
                        genericAliases: {
                            recursive: {
                                of: ["?", "completely", "recursive", "types"],
                                line: "Test Line A"
                            }
                        }
                    })
                }).toThrow({
                    message: "infiniteLoopInGenericAlias",
                    line: "Test Line A"
                })
            })            
            
            it("throws an exception should a two-chain-alias loop exist", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            notRecursive: {
                                of: ["totally", "different", "types"]
                            }
                        },
                        genericAliases: {
                            infinite: {
                                of: ["?", "completely", "loop", "types"],
                                line: "Test Line B"
                            },
                            loop: {
                                of: ["?", "infinite", "loop", "types"],
                                line: "Test Line B"
                            }                            
                        }
                    })
                }).toThrow({
                    message: "infiniteLoopInGenericAlias",
                    line: "Test Line B"
                })
            })
            
            it("throws an exception should a two-chain-alias loop exist ignoring its presence in base type aliases", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            notRecursive: {
                                of: ["totally", "loop", "types"]
                            }
                        },
                        genericAliases: {
                            infinite: {
                                of: ["?", "completely", "loop", "types"],
                                line: "Test Line B"
                            },
                            loop: {
                                of: ["?", "infinite", "loop", "types"],
                                line: "Test Line B"
                            }                            
                        }
                    })
                }).toThrow({
                    message: "infiniteLoopInGenericAlias",
                    line: "Test Line B"
                })
            })                        
            
            it("throws an exception should a three-chain-alias loop exist", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            notRecursive: {
                                of: ["totally", "different", "types"]
                            }
                        },
                        genericAliases: {
                            infinite: {
                                of: ["?", "completely", "loop", "types"],
                                line: "Test Line B"
                            },
                            spin: {
                                of: ["?", "utterly", "loop"],
                                line: "Test Line B"
                            },
                            loop: {
                                of: ["?", "infinite", "roundtrip", "types"],
                                line: "Test Line B"
                            }                            
                        }
                    })
                }).toThrow({
                    message: "infiniteLoopInGenericAlias",
                    line: "Test Line B"
                })
            })
            
            it("throws an exception should a three-chain-alias loop exist ignoring its presence in base type aliases", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            notRecursive: {
                                of: ["totally", "spin", "types"]
                            }
                        },
                        genericAliases: {
                            infinite: {
                                of: ["?", "completely", "loop", "types"],
                                line: "Test Line B"
                            },
                            spin: {
                                of: ["?", "utterly", "loop"],
                                line: "Test Line B"
                            },
                            loop: {
                                of: ["?", "infinite", "roundtrip", "types"],
                                line: "Test Line B"
                            }                            
                        }
                    })
                }).toThrow({
                    message: "infiniteLoopInGenericAlias",
                    line: "Test Line B"
                })
            })                        
            
            it("throws an exception should a non-looping base alias reference a looping base alias", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            external: {
                                of: ["spin"]
                            },
                            infinite: {
                                line: "Test Line A",
                                of: ["spin"]
                            },
                            spin: {
                                line: "Test Line A",
                                of: ["loop"]
                            },
                            loop: {
                                line: "Test Line A",
                                of: ["infinite"]
                            }                            
                        },
                        genericAliases: {
                            nonRecursive: {
                                of: ["?", "completely", "different", "types"]
                            }
                        }
                    })
                }).toThrow({
                    line: "Test Line A",
                    message: "infiniteLoopInNonGenericAlias"
                })
            })
            
            it("throws an exception should a non-looping chain alias reference a looping chain alias", function(){
                expect(function(){
                    SUNRUSE.influx().checkForAliasLoops({
                        aliases: {
                            notRecursive: {
                                of: ["totally", "different", "types"]
                            }
                        },
                        genericAliases: {
                            external: {
                                of: ["?", "infinite"],
                            },
                            infinite: {
                                of: ["?", "completely", "loop", "types"],
                                line: "Test Line B"
                            },
                            spin: {
                                of: ["?", "utterly", "loop"],
                                line: "Test Line B"
                            },
                            loop: {
                                of: ["?", "infinite", "roundtrip", "types"],
                                line: "Test Line B"
                            }                            
                        }
                    })
                }).toThrow({
                    message: "infiniteLoopInGenericAlias",
                    line: "Test Line B"
                })
            })            
        })
    })
})