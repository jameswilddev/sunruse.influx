describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("typecheckChain", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "typecheckChainFunctions")
                instance.primitives = {
                    primitiveA: {
                        regex: /^valueFromPrimitiveA$/
                    },
                    primitiveB: {
                        regex: /^valueFromPrimitiveB$/
                    },
                    primitiveC: {
                        regex: /^valueFromPrimitiveC$/
                    }                   
                }    
            })
            
            it("throws an exception when an unresolvable let is given", function(){
                expect(function(){
                    instance.typecheckChain({
                        line: "test line",
                        let: "letE", 
                        functions: "test functions"
                    }, {
                        takes: "test taken type",
                        lets: {
                            letA: {
                                returns: "test let type A"
                            },
                            letB: {
                                returns: "test let type B"
                            },
                            letC: {
                                returns: "test let type C"
                            },                            
                            letD: {
                                returns: "test let type D"
                            }                            
                        }
                    }, "test output type", "test parsed types")
                }).toThrow({
                    message: "inputUnresolvable",
                    line: "test line"
                })
            })
            
            it("passes let types down to typecheckChainFunction", function(){
                instance.typecheckChain({
                    line: "test line",
                    let: "letB", 
                    functions: "test functions"
                }, {
                    takes: "test taken type",
                    lets: {
                        letA: {
                            returns: "test let type A"
                        },
                        letB: {
                            returns: "test let type B"
                        },
                        letC: {
                            returns: "test let type C"
                        },                            
                        letD: {
                            returns: "test let type D"
                        }                            
                    }
                }, "test output type", "test parsed types")
                expect(instance.typecheckChainFunctions).toHaveBeenCalledWith("test let type B", "test functions", "test output type", "test line", "test parsed types")
            })
            
            it("passes primitive types down to typecheckChainFunction", function(){
                instance.typecheckChain({
                    line: "test line",
                    primitive: "valueFromPrimitiveB", 
                    functions: "test functions"
                }, {
                    takes: "test taken type",
                    lets: {
                        letA: {
                            returns: "test let type A"
                        },
                        letB: {
                            returns: "test let type B"
                        },
                        letC: {
                            returns: "test let type C"
                        },                            
                        letD: {
                            returns: "test let type D"
                        }                            
                    }
                }, "test output type", "test parsed types")
                expect(instance.typecheckChainFunctions).toHaveBeenCalledWith(["primitiveB"], "test functions", "test output type", "test line", "test parsed types")
            })            
            
            it("passes the input type down to typecheckChainFunction", function(){
                instance.typecheckChain({
                    line: "test line",
                    input: true, 
                    functions: "test functions"
                }, {
                    takes: "test taken type",
                    lets: {
                        letA: {
                            returns: "test let type A"
                        },
                        letB: {
                            returns: "test let type B"
                        },
                        letC: {
                            returns: "test let type C"
                        },                            
                        letD: {
                            returns: "test let type D"
                        }                            
                    }
                }, "test output type", "test parsed types")
                expect(instance.typecheckChainFunctions).toHaveBeenCalledWith("test taken type", "test functions", "test output type", "test line", "test parsed types")
            })                        
        })
    })
})