#SUNRUSE.influx
A simple, cut-down constant-time pure functional programming language, designed to be used as part of software written in other languages.

### What can it do?
* Compile and run in any JavaScript host, including your web browser.
* Generate code from functions for almost any language.

### What can't it do?
* Create standalone applications.
* Process data which cannot be stored in constant time, i.e. non-fixed-size arrays.

### What is it for?
* Generating shader code.
* Functional programming for embedded systems.
* Erm...

## Using SUNRUSE.influx
Include or reference SUNRUSE.influx.js.  This will create the SUNRUSE.influx global function, which may be called to get a new instance of the toolchain object.

### Syntax
Though the language is case-insensitive, case used at declaration time will be preserved for generated code.  Typically, camelCase is used throughout.

Names, used to identify types and functions, can be any length, and contain any non-whitespace character other than (brackets).

#### Reserved keywords
The following keywords are defined by the language and cannot be used for declaration names:

* let
* combines
* takes
* returns
* ?

#### Primitive types
Support for the following primitive types is included:

| Name  | Description           | Example constants              |
|-------|-----------------------|--------------------------------|
| int   | 32-bit signed integer | 0 3 -4                         |
| float | 32-bit IEEE float     | 0.0 0.1 -0.2 3.4 -4.6 5.0 -7.0 |
| bool  | A single flag\bit.    | true false                     |

#### Alias types
Aliases are alternative terms for types; they expand to their aliased type at compile time, but are useful in that they can help describe the units/purpose of a field.  To define an alias, use the following syntax:

    {name} aliases {aliased type}
    
You may also make a generic alias as follows:

    ? {name} aliases {aliased type}
   
An example of how this may be used:

    healthPoints aliases int
    ? limit aliases ?
    # The type reference "healthPoints limit" now refers to an int.

#### Combination types
To declare a combination type, use the following syntax:

    {name} combines
        {field name} {field type}
        {field name} {field type}
        ...
        
You may also make a generic combination type as follows:

    ? {name} combines
        {field name} {field type}
        {field name} ?
        ...

Generic combination types may be stacked.  The type "triplet of pairs of floats" would be written:

    float pair triplet

#### Functions
All functions take one input and return one output.  Combination fields may be used to provide a function with more than one piece of information at a time.  To define a function, use the following syntax:

    {name} takes {input type} returns {output type}
        {"input" or a constant} {one or more function names}
        {"input" or a constant} {one or more function names}
        ...
        
Should additional calculations be required, temporary variables can be stored using the "let" keyword:

    {name} takes {input type} returns {output type}
        let {name} {"input", the name of a let, or a constant} {one or more function names}
        let {name} {"input", the name of a let, or a constant} {one or more function names}
        {"input", the name of a let, or a constant} {one or more function names}
        {"input", the name of a let, or a constant} {one or more function names}
        ...
        
An open generic may also be used in the input type.