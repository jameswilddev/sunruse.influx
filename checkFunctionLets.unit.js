describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("checkFunctionLets", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "typecheckFieldChains")
            })
            
            describe("when no lets exist", function(){
                it("does nothing", function(){
                    instance.checkFunctionLets({
                        lets: {}
                    })
                })
            })
            
            describe("when valid lets exist", function(){
                it("only typechecks the lets", function(){
                    var func = {
                        lets: {
                            letA: {
                                let: "letC",
                                line: "test line a"
                            },
                            letC: {
                                let: "letD",
                                line: "test line c"
                            },
                            letD: {
                                primitive: "3.5",
                                line: "test line d"
                            },
                            letB: {
                                let: "letC",
                                line: "test line b"
                            },
                            letE: {
                                input: true,
                                line: "test line e"
                            }
                        }
                    }
                    instance.checkFunctionLets(func, "test parsed types")
                    expect(instance.typecheckFieldChains).toHaveBeenCalledWith(func.lets.letA, func, "test parsed types")
                    expect(instance.typecheckFieldChains).toHaveBeenCalledWith(func.lets.letB, func, "test parsed types")
                    expect(instance.typecheckFieldChains).toHaveBeenCalledWith(func.lets.letC, func, "test parsed types")
                    expect(instance.typecheckFieldChains).toHaveBeenCalledWith(func.lets.letD, func, "test parsed types")
                    expect(instance.typecheckFieldChains).toHaveBeenCalledWith(func.lets.letE, func, "test parsed types")
                })
            })
            
            it("does not crash given unresolvable lets", function(){
                instance.checkFunctionLets({
                    lets: {
                        letA: {
                            let: "letC",
                            line: "test line a"
                        },
                        letC: {
                            let: "letD",
                            line: "test line c"
                        },
                        letD: {
                            primitive: "3.5",
                            line: "test line d"
                        },
                        letB: {
                            let: "letX",
                            line: "test line b"
                        },
                        letE: {
                            input: true,
                            line: "test line e"
                        }
                    }
                }, "test parsed types")
            })
            
            describe("when a circular reference between a let and itself exists", function(){
                it("throws an exception", function(){
                    expect(function(){
                        instance.checkFunctionLets({
                            lets: {
                                letA: {
                                    let: "letA",
                                    line: "test line a"
                                },
                                letC: {
                                    let: "letD",
                                    line: "test line c"
                                },
                                letD: {
                                    primitive: "3.5",
                                    line: "test line d"
                                },
                                letB: {
                                    let: "letC",
                                    line: "test line b"
                                },
                                letE: {
                                    input: true,
                                    line: "test line e"
                                }
                            }
                        })
                    }).toThrow({
                        message: "circularDependencyInLets",
                        line: "test line a"
                    })
                })
            })
            
            describe("when a circular reference between two lets exists", function(){
                it("throws an exception", function(){
                    expect(function(){
                        instance.checkFunctionLets({
                            lets: {
                                letA: {
                                    let: "letC",
                                    line: "test line a"
                                },
                                letC: {
                                    let: "letA",
                                    line: "test line a"
                                },
                                letD: {
                                    primitive: "3.5",
                                    line: "test line d"
                                },
                                letB: {
                                    let: "letC",
                                    line: "test line b"
                                },
                                letE: {
                                    input: true,
                                    line: "test line e"
                                }
                            }
                        })
                    }).toThrow({
                        message: "circularDependencyInLets",
                        line: "test line a"
                    })
                })
            })
            
            describe("when a circular reference between three lets exists", function(){
                it("throws an exception", function(){
                    expect(function(){
                        instance.checkFunctionLets({
                            lets: {
                                letA: {
                                    let: "letB",
                                    line: "test line a"
                                },
                                letC: {
                                    let: "letA",
                                    line: "test line a"
                                },
                                letD: {
                                    primitive: "3.5",
                                    line: "test line d"
                                },
                                letB: {
                                    let: "letC",
                                    line: "test line a"
                                },
                                letE: {
                                    input: true,
                                    line: "test line e"
                                }
                            }
                        })
                    }).toThrow({
                        message: "circularDependencyInLets",
                        line: "test line a"
                    })
                })
            })
        })
    })
})