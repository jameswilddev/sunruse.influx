describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("expandAllAliases", function(){
            var instance, parsed
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "expandAliases")
                
                parsed = {
                    functions: {
                        functionA: {
                            takes: "functionAtakes",
                            returns: "functionAreturns",
                            lets: {
                                letAA: {
                                    returns: "functionAletAtype"
                                },
                                letAB: {
                                    returns: "functionAletBtype"
                                }                                
                            }
                        },
                        functionB: {
                            takes: "functionBtakes",
                            returns: "functionBreturns",
                            lets: {
                                letAA: {
                                    returns: "functionBletAtype"
                                },
                                letAB: {
                                    returns: "functionBletBtype"
                                }                                
                            }
                        }                        
                    },
                    genericFunctions: {
                        functionC: {
                            takes: "functionCtakes",
                            returns: "functionCreturns",
                            lets: {
                                letCA: {
                                    returns: "functionCletAtype"
                                },
                                letCB: {
                                    returns: "functionCletBtype"
                                }                                
                            }
                        },
                        functionD: {
                            takes: "functionDtakes",
                            returns: "functionDreturns",
                            lets: {
                                letDA: {
                                    returns: "functionDletAtype"
                                },
                                letDB: {
                                    returns: "functionDletBtype"
                                }                                
                            }
                        }                        
                    },
                    combinations: {
                        combinationA: {
                            fields: {
                                fieldAA: {
                                    of: "combinationAA"
                                },
                                fieldAB: {
                                    of: "combinationAB"
                                }
                            }
                        },
                        combinationB: {
                            fields: {
                                fieldBA: {
                                    of: "combinationBA"
                                },
                                fieldBB: {
                                    of: "combinationBB"
                                }
                            }
                        }                        
                    },
                    genericCombinations: {
                        combinationC: {
                            fields: {
                                fieldCA: {
                                    of: "combinationCA"
                                },
                                fieldCB: {
                                    of: "combinationCB"
                                }
                            }
                        },
                        combinationD: {
                            fields: {
                                fieldDA: {
                                    of: "combinationDA"
                                },
                                fieldDB: {
                                    of: "combinationDB"
                                }
                            }
                        }                        
                    }                    
                }
                instance.expandAllAliases(parsed)
            })
            
            it("expands aliases in generic function inputs", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("functionCtakes", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionDtakes", parsed)
            })
            
            it("expands aliases in non-generic function inputs", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("functionAtakes", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionBtakes", parsed)
            })            
            
            it("expands aliases in generic function outputs", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("functionCreturns", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionDreturns", parsed)
            })
            
            it("expands aliases in non-generic function outputs", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("functionAreturns", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionBreturns", parsed)
            })                        
            
            it("expands aliases in generic combination fields", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationCA", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationCB", parsed)
                
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationDA", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationDB", parsed)                
            })
            
            it("expands aliases in non-generic combination fields", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationAA", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationAB", parsed)
                
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationBA", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("combinationBB", parsed)                
            })            
            
            it("expands aliases in generic function let types", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("functionCletAtype", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionCletBtype", parsed)
                
                expect(instance.expandAliases).toHaveBeenCalledWith("functionDletAtype", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionDletBtype", parsed)                
            })
            
            it("expands aliases in non-generic function let types", function(){
                expect(instance.expandAliases).toHaveBeenCalledWith("functionAletAtype", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionAletBtype", parsed)
                
                expect(instance.expandAliases).toHaveBeenCalledWith("functionBletAtype", parsed)
                expect(instance.expandAliases).toHaveBeenCalledWith("functionBletBtype", parsed)                
            })
        })
    })
})