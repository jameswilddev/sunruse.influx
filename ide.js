$(document).ready(function(){
    var code = $(".source > .code")
    var status = $(".status")
    var correction = $(".correction")
    var generated = $(".generated > .code")
    var codeChange = function(){
        // We need to expand the code view based on the number of lines.
        code.outerHeight(code.val().split("\n").length + 2 + "rem")
        
        correction.hide()
        
        var instance = SUNRUSE.influx()
        SUNRUSE.influx.compilers.glsl(instance)
        
        instance.build([{
            filename: "source",
            source: code.val()
        }], function(name, completed, total){
            status.text("(" + completed + "/" + total + ") - starting " + name)
        }, function(name, completed, total){

        }, function(result, runtime){
            status.text("completed successfully in " + runtime + "msec")
            generated.val(JSON.stringify(result))
        }, function(line, message, runtime, completed, total) {
            correction.css({ top: line.line + "rem" })
            correction.text(message)
            correction.show()
            status.text("failed with \"" + message + "\" on line " + line.line + " of file \"" + line.filename + "\" (" + completed + "/" + total + "; total " + runtime + "msec)")
        })
    }
    code.on("input", codeChange)
    codeChange()
})