describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("typecheckChainFunctions", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
            })
            
            describe("when there is a chain", function(){
                beforeEach(function(){
                    spyOn(instance, "findFunctionByInputAndName").and.callFake(function(input, name, parsed){
                        expect(parsed).toEqual("test parsed types")
                        switch(name) {
                            case "chain":
                                expect(input).toEqual(["test", "input", "type"])
                                return {
                                    returns: ["test", "chained", "type"]
                                }
                            case "functions":
                                expect(input).toEqual(["test", "chained", "type"])
                                return {
                                    returns: ["test", "additional", "chained", "type"]
                                }
                        }
                    })
                    spyOn(instance, "findFieldByInputAndName").and.callFake(function(input, name, parsed){
                        expect(parsed).toEqual("test parsed types")
                        switch(name) {
                            case "and":
                                expect(input).toEqual(["test", "additional", "chained", "type"])
                                return {
                                    of: ["test", "field", "type"]
                                }
                            case "fields":
                                expect(input).toEqual(["test", "field", "type"])
                                return {
                                    of: ["test", "output", "type"]
                                }
                        }
                    })
                })
                
                it("does nothing when the chain is resolvable and the return type matches", function(){
                    instance.typecheckChainFunctions(["test", "input", "type"], ["chain", "functions", "and", "fields"], ["test", "output", "type"], "test line", "test parsed types")
                })
                
                it("throws an exception when part of the chain is unresolvable", function(){
                    expect(function(){
                        instance.typecheckChainFunctions(["test", "input", "type"], ["chain", "unknowns", "and", "fields"], ["test", "output", "type"], "test line", "test parsed types")
                    }).toThrow({
                        message: "functionInChainUnresolvable",
                        name: "unknowns",
                        taking: ["test", "chained", "type"],
                        line: "test line"
                    })
                })
                
                it("throws an exception when the resulting type does not match the return type in length", function(){
                    expect(function(){
                        instance.typecheckChainFunctions(["test", "input", "type"], ["chain", "functions", "and", "fields"], ["test", "output"], "test line", "test parsed types")
                    }).toThrow({
                        message: "typeAtEndOfChainIncorrect",
                        expected: ["test", "output"],
                        actual: ["test", "output", "type"],
                        line: "test line"
                    })
                })
                
                it("throws an exception when the resulting type does not match the return type", function(){
                    expect(function(){
                        instance.typecheckChainFunctions(["test", "input", "type"], ["chain", "functions", "and", "fields"], ["test", "output", "fish"], "test line", "test parsed types")
                    }).toThrow({
                        message: "typeAtEndOfChainIncorrect",
                        expected: ["test", "output", "fish"],
                        actual: ["test", "output", "type"],
                        line: "test line"
                    })
                })                
            })
        })
    })
})