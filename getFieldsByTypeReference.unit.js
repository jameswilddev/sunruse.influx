describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("getFieldsByTypeReference", function(){
            var instance, types
            beforeEach(function(){
                instance = SUNRUSE.influx()
                instance.primitives = {
                    primitiveA: {
                        regex: /^valueFromPrimitiveA$/
                    },
                    primitiveB: {
                        regex: /^valueFromPrimitiveB$/
                    },
                    primitiveC: {
                        regex: /^valueFromPrimitiveC$/
                    }                   
                } 
                types = {
                    genericCombinations: {
                        genericA: {
                            fields: {
                                fieldAA: {
                                    of: ["?", "chainAA", "followingAA"]
                                },                            
                                fieldAB: {
                                    of: ["baseAC"]
                                },
                                fieldAC: {
                                    of: ["baseAB", "followingAB"]
                                }
                            },
                            fieldsOrder: ["fieldAA", "fieldAB", "fieldAC"]
                        },
                        genericB: {
                            fields: {
                                fieldBA: {
                                    of: ["?", "chainBA", "followingBA"]
                                },                            
                                fieldBB: {
                                    of: ["baseBC"]
                                },
                                fieldBC: {
                                    of: ["baseBB", "followingBB"]
                                },
                                fieldBD: {
                                    of: ["?"]
                                },                                                        
                            },
                            fieldsOrder: ["fieldBA", "fieldBC", "fieldBB", "fieldBD"]
                        }                    
                    },
                    combinations: {
                        nongenericC: {
                            fields: {
                                fieldCA: {
                                    of: ["baseCB"]
                                },                            
                                fieldCB: {
                                    of: ["baseCA", "followingCA"]
                                }
                            },
                            fieldsOrder: ["fieldCB", "fieldCA"]
                        },
                        nongenericD: {
                            fields: {
                                fieldDA: {
                                    of: ["baseDA"]
                                },                            
                                fieldDB: {
                                    of: ["baseDB", "followingBA"]
                                }
                            },
                            fieldsOrder: ["fieldDA", "fieldDB"]
                        }                    
                    }
                }
            })
            
            it("returns the type verbatim when an open generic is given", function(){
                expect(instance.getFieldsByTypeReference(["?"], types)).toEqual([["?"]])
            })
            
            it("returns the primitive type when a literal is given", function(){
                expect(instance.getFieldsByTypeReference(["primitiveB"], types)).toEqual([["primitiveB"]])
            })            
            
            it("returns the combination's fields when a nongeneric combination is given", function(){
                expect(instance.getFieldsByTypeReference(["nongenericC"], types)).toEqual([["baseCA", "followingCA"], ["baseCB"]])
            })
            
            it("returns the combination's fields when an open generic combination is given", function(){
                expect(instance.getFieldsByTypeReference(["?", "genericB"], types)).toEqual([["?", "chainBA", "followingBA"], ["baseBB", "followingBB"], ["baseBC"], ["?"]])
            })
            
            it("returns the combination's fields when a closed generic combination is given", function(){
                expect(instance.getFieldsByTypeReference(["int", "genericB"], types)).toEqual([["int", "chainBA", "followingBA"], ["baseBB", "followingBB"], ["baseBC"], ["int"]])
            })            
            
            it("returns the combination's fields when a closed generic combination is given with a chain inside", function(){
                expect(instance.getFieldsByTypeReference(["int", "extraLink", "genericB"], types)).toEqual([["int", "extraLink", "chainBA", "followingBA"], ["baseBB", "followingBB"], ["baseBC"], ["int", "extraLink"]])
            })                        
        })
    })
})