describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("expandAliases", function(){
            it("does nothing when no aliases are given", function(){
                var type = ["?", "which", "is", "unchanged"]
                var parsed = {
                    aliases: {},
                    genericAliases: {}
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["?", "which", "is", "unchanged"])
            })
            
            it("does nothing when none of the given aliases match", function(){
                var type = ["?", "which", "is", "unchanged"]
                var parsed = {
                    aliases: {
                        doNotInclude: {
                            of: ["should", "never", "be", "used"]
                        }
                    },
                    genericAliases: {
                        alsoDoNotInclude: {
                            of: ["?", "should", "never", "be", "seen"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["?", "which", "is", "unchanged"])
            })
            
            it("replaces the base type when an alias matches", function(){
                var type = ["base", "chainA", "chainB"]
                var parsed = {
                    aliases: {
                        base: {
                            of: ["expand", "to"]
                        }
                    },
                    genericAliases: {
                        alsoDoNotInclude: {
                            of: ["?", "should", "never", "be", "seen"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["expand", "to", "chainA", "chainB"])
            })
            
            it("does not replace the base type when it matches a generic alias", function(){
                var type = ["base", "which", "is", "unchanged"]
                var parsed = {
                    aliases: {
                        doNotInclude: {
                            of: ["should", "never", "be", "used"]
                        }
                    },
                    genericAliases: {
                        base: {
                            of: ["?", "should", "never", "be", "seen"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["base", "which", "is", "unchanged"])
            })
            
            it("replaces wrapping types when aliases match", function(){
                var type = ["base", "with", "a", "chain"]
                var parsed = {
                    aliases: {
                        doNotInclude: {
                            of: ["should", "never", "be", "used"]
                        }
                    },
                    genericAliases: {
                        a: {
                            of: ["?", "replaced", "parts"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["base", "with", "replaced", "parts", "chain"])
            })
            
            it("does not replace wrapping types when non-generic aliases match", function(){
                var type = ["base", "with", "a", "chain"]
                var parsed = {
                    aliases: {
                        a: {
                            of: ["should", "never", "be", "used"]
                        }
                    },
                    genericAliases: {
                        doNotInclude: {
                            of: ["?", "replaced", "parts"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["base", "with", "a", "chain"])
            })
            
            it("can replace both the base and stacked types", function(){
                var type = ["base", "with", "a", "chain"]
                var parsed = {
                    aliases: {
                        base: {
                            of: ["expanded", "floor"]
                        }
                    },
                    genericAliases: {
                        a: {
                            of: ["?", "replaced", "parts"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["expanded", "floor", "with", "replaced", "parts", "chain"])
            })
            
            it("can recursively replace the base type", function(){
                var type = ["base", "chainA", "chainB"]
                var parsed = {
                    aliases: {
                        further: {
                            of: ["itWasBefore"]
                        },
                        expand: {
                            of: ["further", "than"]
                        },
                        base: {
                            of: ["expand", "to"]
                        }
                    },
                    genericAliases: {
                        alsoDoNotInclude: {
                            of: ["?", "should", "never", "be", "seen"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["itWasBefore", "than", "to", "chainA", "chainB"])
            })
            
            it("can recursively replace stacked types", function(){
                var type = ["base", "with", "a", "chain"]
                var parsed = {
                    aliases: {
                        doNotInclude: {
                            of: ["should", "never", "be", "used"]
                        }
                    },
                    genericAliases: {
                        a: {
                            of: ["?", "replaced", "parts"]
                        },
                        replaced: {
                            of: ["?", "one", "level", "deep"]
                        },
                        parts: {
                            of: ["?", "many"]
                        },
                        many: {
                            of: ["?", "deeper", "levels"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["base", "with", "one", "level", "deep", "deeper", "levels", "chain"])
            })
            
            it("can recursively replace stacked types generated from the base type", function(){
                var type = ["base", "chainA", "chainB"]
                var parsed = {
                    aliases: {
                        base: {
                            of: ["expand", "to"]
                        }
                    },
                    genericAliases: {
                        to: {
                            of: ["?", "a", "subType"]
                        }
                    }
                }
                
                SUNRUSE.influx().expandAliases(type, parsed)
                
                expect(type).toEqual(["expand", "a", "subType", "chainA", "chainB"])                
            })            
        })
    })
})