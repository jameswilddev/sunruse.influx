describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("checkUniqueness", function(){
            var instance, run, types
            beforeEach(function(){
                instance = SUNRUSE.influx()
                types = {
                    aliases: {},
                    genericAliases: {},
                    combinations: {},
                    genericCombinations: {},
                    functions: []
                }
                run = function(){
                    instance.checkUniqueness(types)
                }
            })
            
            it("throws an exception when a nongeneric alias and combination clash", function(){
                types.aliases.testType = { line: "Test Line" }
                
                types.combinations.testType = { line: "Test Line" }
                
                expect(run).toThrow({
                    line: "Test Line",
                    message: "typeNamesNotUnique"
                })
            })
            
            it("throws an exception when a generic alias and combination clash", function(){
                types.genericAliases.testType = { line: "Test Line" }
                
                types.genericCombinations.testType = { line: "Test Line" }
                
                expect(run).toThrow({
                    line: "Test Line",
                    message: "typeNamesNotUnique"
                })
            })            
            
            it("does not throw an exception when a generic and nongeneric alias of the same name exist", function(){
                types.aliases.testType = { line: "Test Line" }
                types.genericAliases.testType = { line: "Test Line" }
                
                run()
            })
            
            it("does not throw an exception when a generic and nongeneric combination of the same name exist", function(){
                types.combinations.testType = { line: "Test Line" }
                types.genericCombinations.testType = { line: "Test Line" }
                
                run()
            })            
            
            it("does not throw an exception when a generic alias and nongeneric combination of the same name exist", function(){
                types.combinations.testType = { line: "Test Line" }
                types.genericAliases.testType = { line: "Test Line" }
                
                run()
            })                        
            
            it("does not throw an exception when a nongeneric alias and generic combination of the same name exist", function(){
                types.aliases.testType = { line: "Test Line" }
                types.genericCombinations.testType = { line: "Test Line" }
                
                run()
            })                                    
        })
    })
})