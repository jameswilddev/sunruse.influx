describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("findFunctionByInputAndName", function(){
            var instance, types, match, matchResult
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "typeReferencesMatch").and.callFake(function(a, b){
                    expect(a).not.toEqual(b)
                    if(a != match && b != match) return
                    if(a != "Test Query Type" && b != "Test Query Type") return matchResult
                    return matchResult
                })
                instance.nativeFunctions = [{
                    name: "Test Native Function A",
                    takes: ["?", "generic", "inputs"],
                    returns: ["?", "generic", "outputs", "longer"]
                }, {
                    name: "Test Native Function B",
                    takes: ["?", "generic", "input", "type"],
                    returns: ["nongeneric", "return"]
                }, {
                    name: "Test Native Function C",
                    takes: ["takes", "nongeneric"],
                    returns: ["?", "returns", "generic"]
                }, {
                    name: "Test Native Function D",
                    takes: ["nongeneric", "taken"],
                    returns: ["returns", "nongeneric"]
                }, {
                    name: "Test Native Function E",
                    takes: ["?"],
                    returns: ["type", "anything"]
                }, {
                    name: "Test Native Function F",
                    takes: ["type", "anything"],
                    returns: ["?"]
                }]
                types = {
                    functions: [{
                        name: "Test Function A",
                        takes: ["?", "generic", "inputs"],
                        returns: ["?", "generic", "outputs", "longer"]
                    }, {
                        name: "Test Function B",
                        takes: ["?", "generic", "input", "type"],
                        returns: ["nongeneric", "return"]
                    }, {
                        name: "Test Function C",
                        takes: ["takes", "nongeneric"],
                        returns: ["?", "returns", "generic"]
                    }, {
                        name: "Test Function D",
                        takes: ["nongeneric", "taken"],
                        returns: ["returns", "nongeneric"]
                    }, {
                        name: "Test Function E",
                        takes: ["?"],
                        returns: ["type", "anything"]
                    }, {
                        name: "Test Function F",
                        takes: ["type", "anything"],
                        returns: ["?"]
                    }]
                }
            })
            
            describe("native functions", function(){
                it("returns the matching function with takes and returns substituted", function(){
                    match = instance.nativeFunctions[0].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Native Function A", types)).toEqual({
                        function: instance.nativeFunctions[0],
                        takes: ["base", "genericType", "generic", "inputs"],
                        returns: ["base", "genericType", "generic", "outputs", "longer"]
                    })
                })
                
                it("returns the matching function with only takes substituted", function(){
                    match = instance.nativeFunctions[1].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Native Function B", types)).toEqual({
                        function: instance.nativeFunctions[1],
                        takes: ["base", "genericType", "generic", "input", "type"],
                        returns: ["nongeneric", "return"]
                    })
                })            
                
                it("returns the matching function with only returns substituted", function(){
                    match = instance.nativeFunctions[2].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Native Function C", types)).toEqual({
                        function: instance.nativeFunctions[2],
                        takes: ["takes", "nongeneric"],
                        returns: ["base", "genericType", "returns", "generic"]
                    })
                })              
                
                it("returns the matching function without any substitutions", function(){
                    match = instance.nativeFunctions[3].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Native Function D", types)).toEqual({
                        function: instance.nativeFunctions[3],
                        takes: ["nongeneric", "taken"],
                        returns: ["returns", "nongeneric"]
                    })
                })                          
                
                it("returns the matching function with takes substituted from an open generic", function(){
                    match = instance.nativeFunctions[4].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Native Function E", types)).toEqual({
                        function: instance.nativeFunctions[4],
                        takes: ["base", "genericType"],
                        returns: ["type", "anything"]
                    })
                })                                      
                
                it("returns the matching function with returns substituted from an open generic", function(){
                    match = instance.nativeFunctions[5].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Native Function F", types)).toEqual({
                        function: instance.nativeFunctions[5],
                        takes: ["type", "anything"],
                        returns: ["base", "genericType"]
                    })
                })                                                  
            })
            
            describe("non-native functions", function(){
                it("returns the matching function with takes and returns substituted", function(){
                    match = types.functions[0].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Function A", types)).toEqual({
                        function: types.functions[0],
                        takes: ["base", "genericType", "generic", "inputs"],
                        returns: ["base", "genericType", "generic", "outputs", "longer"]
                    })
                })
                
                it("returns the matching function with only takes substituted", function(){
                    match = types.functions[1].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Function B", types)).toEqual({
                        function: types.functions[1],
                        takes: ["base", "genericType", "generic", "input", "type"],
                        returns: ["nongeneric", "return"]
                    })
                })            
                
                it("returns the matching function with only returns substituted", function(){
                    match = types.functions[2].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Function C", types)).toEqual({
                        function: types.functions[2],
                        takes: ["takes", "nongeneric"],
                        returns: ["base", "genericType", "returns", "generic"]
                    })
                })              
                
                it("returns the matching function without any substitutions", function(){
                    match = types.functions[3].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Function D", types)).toEqual({
                        function: types.functions[3],
                        takes: ["nongeneric", "taken"],
                        returns: ["returns", "nongeneric"]
                    })
                })                          
                
                it("returns the matching function with takes substituted from an open generic", function(){
                    match = types.functions[4].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Function E", types)).toEqual({
                        function: types.functions[4],
                        takes: ["base", "genericType"],
                        returns: ["type", "anything"]
                    })
                })                                      
                
                it("returns the matching function with returns substituted from an open generic", function(){
                    match = types.functions[5].takes
                    matchResult = ["base", "genericType"]
                    
                    expect(instance.findFunctionByInputAndName("Test Query Type", "Test Function F", types)).toEqual({
                        function: types.functions[5],
                        takes: ["type", "anything"],
                        returns: ["base", "genericType"]
                    })
                })                                                  
            })
        })
    })
})