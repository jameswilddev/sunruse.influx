describe("SUNRUSE", function(){
    describe("influx", function(){
        var instance
        beforeEach(function(){
            instance = SUNRUSE.influx()
        })
        
        it("defines no primitives by default", function(){
            expect(instance.primitives).toEqual({})
        })        
        
        describe("getPrimitiveTypeNameByLiteral", function(){
            beforeEach(function(){
                instance.primitives = {
                    primitiveA: {
                        regex: /^valueFromPrimitiveA$/
                    },
                    primitiveB: {
                        regex: /^valueFromPrimitiveB$/
                    },
                    primitiveC: {
                        regex: /^valueFromPrimitiveC$/
                    }                   
                }
            })
            it("returns falsy when no primitive regex matches", function(){
                expect(instance.getPrimitiveTypeNameByLiteral("valueFromPrimitiveD")).toBeFalsy()
            })
            
            it("returns returns the key in the primitives object when a regex matches", function(){
                expect(instance.getPrimitiveTypeNameByLiteral("valueFromPrimitiveB")).toEqual("primitiveB")
            })
        })
    })
})