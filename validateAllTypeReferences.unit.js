describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("validateAllTypeReferences", function(){
            var instance, parsed
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "validateTypeReference")
                
                parsed = {
                    aliases: {
                        aliasA: {
                            line: "aliasAline",
                            of: "aliasAof"
                        },
                        aliasB: {
                            line: "aliasBline",
                            of: "aliasBof"
                        }
                    },
                    genericAliases: {
                        aliasC: {
                            line: "aliasCline",
                            of: "aliasCof"
                        },
                        aliasD: {
                            line: "aliasDline",
                            of: "aliasDof"
                        }
                    },                    
                    functions: {
                        functionA: {
                            takes: "functionAtakes",
                            returns: "functionAreturns",
                            line: "functionAline",
                            lets: {
                                letAA: {
                                    returns: "functionAletAtype",
                                    line: "functionAletAline"
                                },
                                letAB: {
                                    returns: "functionAletBtype",
                                    line: "functionAletBline"
                                }                                
                            }
                        },
                        functionB: {
                            takes: "functionBtakes",
                            returns: "functionBreturns",
                            line: "functionBline",
                            lets: {
                                letAA: {
                                    returns: "functionBletAtype",
                                    line: "functionBletAline"
                                },
                                letAB: {
                                    returns: "functionBletBtype",
                                    line: "functionBletBline"
                                }                                
                            }
                        }                        
                    },
                    genericFunctions: {
                        functionC: {
                            takes: "functionCtakes",
                            returns: "functionCreturns",
                            line: "functionCline",
                            lets: {
                                letCA: {
                                    returns: "functionCletAtype",
                                    line: "functionCletAline"
                                },
                                letCB: {
                                    returns: "functionCletBtype",
                                    line: "functionCletBline"
                                }                                
                            }
                        },
                        functionD: {
                            takes: "functionDtakes",
                            returns: "functionDreturns",
                            line: "functionDline",
                            lets: {
                                letDA: {
                                    returns: "functionDletAtype",
                                    line: "functionDletAline"
                                },
                                letDB: {
                                    returns: "functionDletBtype",
                                    line: "functionDletBline"
                                }                                
                            }
                        }                        
                    },
                    combinations: {
                        combinationA: {
                            fields: {
                                fieldAA: {
                                    of: "combinationAA",
                                    line: "combinationAAline"
                                },
                                fieldAB: {
                                    of: "combinationAB",
                                    line: "combinationABline"
                                }
                            }
                        },
                        combinationB: {
                            fields: {
                                fieldBA: {
                                    of: "combinationBA",
                                    line: "combinationBAline"
                                },
                                fieldBB: {
                                    of: "combinationBB",
                                    line: "combinationBBline"
                                }
                            }
                        }                        
                    },
                    genericCombinations: {
                        combinationC: {
                            fields: {
                                fieldCA: {
                                    of: "combinationCA",
                                    line: "combinationCAline"
                                },
                                fieldCB: {
                                    of: "combinationCB",
                                    line: "combinationCBline"
                                }
                            }
                        },
                        combinationD: {
                            fields: {
                                fieldDA: {
                                    of: "combinationDA",
                                    line: "combinationDAline"
                                },
                                fieldDB: {
                                    of: "combinationDB",
                                    line: "combinationDBline"
                                }
                            }
                        }                        
                    }                    
                }
                instance.validateAllTypeReferences(parsed)
            })
            
            it("validates type references in generic function inputs", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionCtakes", "functionCline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionDtakes", "functionDline", parsed)
            })
            
            it("validates type references in non-generic function inputs", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionAtakes", "functionAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionBtakes", "functionBline", parsed)
            })            
            
            it("validates type references in generic function outputs", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionCreturns", "functionCline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionDreturns", "functionDline", parsed)
            })
            
            it("validates type references in non-generic function outputs", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionAreturns", "functionAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionBreturns", "functionBline", parsed)
            })                        
            
            it("validates type references in generic combination fields", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationCA", "combinationCAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationCB", "combinationCBline", parsed)
                
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationDA", "combinationDAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationDB", "combinationDBline", parsed)                
            })
            
            it("validates type references in non-generic combination fields", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationAA", "combinationAAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationAB", "combinationABline", parsed)
                
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationBA", "combinationBAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("combinationBB", "combinationBBline", parsed)                
            })            
            
            it("validates type references in generic function let types", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionCletAtype", "functionCletAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionCletBtype", "functionCletBline", parsed)
                
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionDletAtype", "functionDletAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionDletBtype", "functionDletBline", parsed)                                
            })
            
            it("validates type references in non-generic function let types", function(){
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionAletAtype", "functionAletAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionAletBtype", "functionAletBline", parsed)
                
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionBletAtype", "functionBletAline", parsed)
                expect(instance.validateTypeReference).toHaveBeenCalledWith("functionBletBtype", "functionBletBline", parsed)                
            })
        })
    })
})