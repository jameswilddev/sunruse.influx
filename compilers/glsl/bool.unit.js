describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("compilers", function(){
            describe("glsl", function(){
                describe("bool", function(){
                    var instance, id, lines
                    beforeEach(function(){
                        SUNRUSE.influx.compilers.glsl(instance = SUNRUSE.influx())
                        var idVal = 0
                        id = function(){
                            switch(idVal++) {
                                case 0: return "testIdA"
                                default: expect(false).toBeTruthy("Too many IDs used")
                            }
                        }
                        lines = ["existing line a", "existing line b"]
                    })
                    
                    describe("literals", function(){
                        describe("true", function(){
                            it("matches", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("true")).toEqual("bool")
                            })
                            
                            it("does not match inside other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("anothertruetoken")).toBeFalsy()
                            })
                            
                            it("does not match at the end of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("anothertrue")).toBeFalsy()
                            })
                            
                            it("does not match at the start of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("truetoken")).toBeFalsy()
                            })                        
                        })                        
                        
                        describe("false", function(){
                            it("matches", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("false")).toEqual("bool")
                            })
                            
                            it("does not match inside other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("anotherfalsetoken")).toBeFalsy()
                            })
                            
                            it("does not match at the end of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("anotherfalse")).toBeFalsy()
                            })
                            
                            it("does not match at the start of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("falsetoken")).toBeFalsy()
                            })                        
                        })
                    })
                    
                    describe("native functions", function(){
                        describe("not", function(){
                            var funct
                            beforeEach(function(){
                                funct = instance.findFunctionByInputAndName(["bool"], "not", { functions: [] }).function
                            })
                            
                            it("generates code for a constant input of false", function(){
                                expect(funct.generateCode({constant:"false"}, id, null, lines)).toEqual({constant:"true"})
                                expect(lines).toEqual(["existing line a", "existing line b"])
                            })
                            
                            it("generates code for a constant input of true", function(){
                                expect(funct.generateCode({constant:"true"}, id, null, lines)).toEqual({constant:"false"})
                                expect(lines).toEqual(["existing line a", "existing line b"])
                            })
                            
                            it("generates code for a nonconstant input", function(){
                                expect(funct.generateCode({reference:"inputVariable"}, id, null, lines)).toEqual({reference: "temp_testIdA"})
                                expect(lines).toEqual(["existing line a", "existing line b", "bool temp_testIdA = !inputVariable;"])
                            })                            
                        })
                    })
                })
            })
        })
    })
})