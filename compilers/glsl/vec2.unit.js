describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("compilers", function(){
            describe("glsl", function(){
                describe("vec4", function(){
                    var instance
                    beforeEach(function(){
                        SUNRUSE.influx.compilers.glsl(instance = SUNRUSE.influx())
                    })                    
                    it("matches", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("vec4(4.67,-799.8,40.0,557.245)")).toEqual("vec4")
                    })
                    
                    it("does not match inside other tokens", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("anothervec4(4.67,-799.8,40.0,557.245)token")).toBeFalsy()
                    })
                    
                    it("does not match at the end of other tokens", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("anothervec4(4.67,-799.8,40.0,557.245)")).toBeFalsy()
                    })
                    
                    it("does not match at the start of other tokens", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("vec4(4.67,-799.8,40.0,557.245)token")).toBeFalsy()
                    })                        
                })
            })
        })
    })
})