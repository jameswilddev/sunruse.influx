describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("compilers", function(){
            describe("glsl", function(){
                describe("float", function(){
                    var instance, id, lines
                    beforeEach(function(){
                        SUNRUSE.influx.compilers.glsl(instance = SUNRUSE.influx())
                        var idVal = 0
                        id = function(){
                            switch(idVal++) {
                                case 0: return "testIdA"
                                default: expect(false).toBeTruthy("Too many IDs used")
                            }
                        }
                        lines = ["existing line a", "existing line b"]
                    })    
                    
                    describe("literals", function(){
                        describe("zero", function(){
                            it("matches", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("0.0")).toEqual("float")
                            })
                            
                            it("does not match inside other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("another0.0token")).toBeFalsy()
                            })
                            
                            it("does not match at the end of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("another0.0")).toBeFalsy()
                            })
                            
                            it("does not match at the start of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("0.0token")).toBeFalsy()
                            })                        
                        })                        
                        
                        describe("positive", function(){
                            it("matches", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("47.554")).toEqual("float")
                            })
                            
                            it("does not match inside other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("another47.554token")).toBeFalsy()
                            })
                            
                            it("does not match at the end of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("another47.554")).toBeFalsy()
                            })
                            
                            it("does not match at the start of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("47.554token")).toBeFalsy()
                            })                        
                        })
                        
                        describe("negative", function(){
                            it("matches", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("-47.554")).toEqual("float")
                            })
                            
                            it("does not match inside other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("another-47.554token")).toBeFalsy()
                            })
                            
                            it("does not match at the end of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("another-47.554")).toBeFalsy()
                            })
                            
                            it("does not match at the start of other tokens", function(){
                                expect(instance.getPrimitiveTypeNameByLiteral("-47.554token")).toBeFalsy()
                            })                        
                        })
                    })
                    
                    describe("native functions", function(){
                        describe("negate", function(){
                            var funct
                            beforeEach(function(){
                                funct = instance.findFunctionByInputAndName(["float"], "negate", { functions: [] }).function
                            })
                            
                            it("negates positive constants", function(){
                                expect(funct.generateCode({constant:"45.78"}, id, null, lines)).toEqual({constant:"-45.78"})
                                expect(lines).toEqual(["existing line a", "existing line b"])
                            })
                            
                            it("negates negative constants", function(){
                                expect(funct.generateCode({constant:"-45.78"}, id, null, lines)).toEqual({constant:"45.78"})
                                expect(lines).toEqual(["existing line a", "existing line b"])
                            })
                            
                            it("negates zero", function(){
                                expect(funct.generateCode({constant:"0.0"}, id, null, lines)).toEqual({constant:"0.0"})
                                expect(lines).toEqual(["existing line a", "existing line b"])
                            })
                            
                            it("negates references", function(){
                                expect(funct.generateCode({reference:"inputVariable"}, id, null, lines)).toEqual({reference: "temp_testIdA"})
                                expect(lines).toEqual(["existing line a", "existing line b", "float temp_testIdA = -inputVariable;"])
                            })
                        })
                    })
                })
            })
        })
    })
})