SUNRUSE.influx.compilers.glsl = function(instance) {
    instance.primitives.int = {
        regex: /^-?[0-9]+$/
    }
    
    instance.primitives.float = {
        regex: /^-?[0-9]+.[0-9]+$/
    }
    
    instance.primitives.bool = {
        regex: /^true$|^false$/
    }
     
    instance.primitives.vec2 = {
        regex: /^vec2\(-?[0-9]+\.[0-9]+,-?[0-9]+\.[0-9]+\)$/
    }
    
    instance.primitives.vec3 = {
        regex: /^vec3\(-?[0-9]+\.[0-9]+,-?[0-9]+\.[0-9]+,-?[0-9]+\.[0-9]+\)$/
    }   
    
    instance.primitives.vec4 = {
        regex: /^vec4\(-?[0-9]+\.[0-9]+,-?[0-9]+\.[0-9]+,-?[0-9]+\.[0-9]+,-?[0-9]+\.[0-9]+\)$/
    }       
    
    instance.nativeFunctions.push({
        name: "not",
        takes: ["bool"],
        returns: ["bool"],
        generateCode: function(input, id, generic, lines){
            switch(input.constant) {
                case "false":
                    return { constant: "true" }
                case "true":
                    return { constant: "false" }                    
                default:
                    var newId = id()
                    lines.push("bool temp_" + newId + " = !" + input.reference + ";")
                    return { reference: "temp_" + newId }
            }
        }
    })
    
    instance.nativeFunctions.push({
        name: "negate",
        takes: ["float"],
        returns: ["float"],
        generateCode: function(input, id, generic, lines){
            if(input.constant == "0.0") return input
            if(input.constant) return { constant: input.constant.charAt(0) != "-" ? "-" + input.constant : input.constant.substring(1) }

            var newId = id()
            lines.push("float temp_" + newId + " = -" + input.reference + ";")
            return { reference: "temp_" + newId }
        }
    })
}