describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("compilers", function(){
            describe("glsl", function(){
                describe("int", function(){
                    var instance
                    beforeEach(function(){
                        SUNRUSE.influx.compilers.glsl(instance = SUNRUSE.influx())
                    })  

                    describe("zero", function(){
                        it("matches", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("0")).toEqual("int")
                        })
                        
                        it("does not match inside other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("another0token")).toBeFalsy()
                        })
                        
                        it("does not match at the end of other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("another0")).toBeFalsy()
                        })
                        
                        it("does not match at the start of other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("0token")).toBeFalsy()
                        })                        
                    })                        
                    
                    describe("positive", function(){
                        it("matches", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("47")).toEqual("int")
                        })
                        
                        it("does not match inside other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("another47token")).toBeFalsy()
                        })
                        
                        it("does not match at the end of other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("another47")).toBeFalsy()
                        })
                        
                        it("does not match at the start of other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("47token")).toBeFalsy()
                        })                        
                    })
                    
                    describe("negative", function(){
                        it("matches", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("-47")).toEqual("int")
                        })
                        
                        it("does not match inside other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("another-47token")).toBeFalsy()
                        })
                        
                        it("does not match at the end of other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("another-47")).toBeFalsy()
                        })
                        
                        it("does not match at the start of other tokens", function(){
                            expect(instance.getPrimitiveTypeNameByLiteral("-47token")).toBeFalsy()
                        })                        
                    })
                })
            })
        })
    })
})