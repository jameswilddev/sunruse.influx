describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("compilers", function(){
            describe("glsl", function(){
                describe("vec2", function(){
                    var instance
                    beforeEach(function(){
                        SUNRUSE.influx.compilers.glsl(instance = SUNRUSE.influx())
                    })                    
                    it("matches", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("vec2(4.67,-799.8)")).toEqual("vec2")
                    })
                    
                    it("does not match inside other tokens", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("anothervec2(4.67,-799.8)token")).toBeFalsy()
                    })
                    
                    it("does not match at the end of other tokens", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("anothervec2(4.67,-799.8)")).toBeFalsy()
                    })
                    
                    it("does not match at the start of other tokens", function(){
                        expect(instance.getPrimitiveTypeNameByLiteral("vec2(4.67,-799.8)token")).toBeFalsy()
                    })                        
                })
            })
        })
    })
})