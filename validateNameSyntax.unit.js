describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("validateNameSyntax", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
                spyOn(instance, "getPrimitiveTypeNameByLiteral")
                instance.primitives = {
                    primitiveA: {
                        regex: /^valueFromPrimitiveA$/
                    },
                    primitiveB: {
                        regex: /^valueFromPrimitiveB$/
                    },
                    primitiveC: {
                        regex: /^valueFromPrimitiveC$/
                    }                   
                }                
            })
            
            var sharedTests = function(allowGenerics){
                it("throws an exception if only a generic is given", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?"], "Test Line", false)
                    }).toThrow({
                        line: "Test Line",
                        message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                    })
                })  
                
                it("throws an exception if the second type is generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["base", "?"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                    })
                })
                
                it("checks if the name is a primitive literal", function(){
                    instance.validateNameSyntax(["base"], "Test Line", allowGenerics)
                    expect(instance.getPrimitiveTypeNameByLiteral).toHaveBeenCalledWith("base")
                })
                
                it("throws an exception if any of the tokens are primitive literals", function(){
                    instance.getPrimitiveTypeNameByLiteral.and.callFake(function(token){
                        return token == "base"
                    })
                    
                    expect(function(){
                        instance.validateNameSyntax(["base"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "namesMayNotContainPrimitiveLiterals"
                    })
                })
                
                it("throws an exception if the name is empty", function(){
                    expect(function(){
                        instance.validateNameSyntax([], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameEmpty"
                    })
                })
                
                it("throws an exception if the name contains more than two tokens", function(){
                    expect(function(){
                        instance.validateNameSyntax(["one", "two", "three"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                    })
                })
                
                it("allows single token type declarations", function(){
                    instance.validateNameSyntax(["test"], "Test Line", allowGenerics)
                })
                
                it("throws an exception given a primitive type's name", function(){
                    expect(function(){
                        instance.validateNameSyntax(["primitiveB"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedPrimitiveTypeGiven"
                    })
                })

                it("throws an exception if the name contains the input keyword", function(){
                    expect(function(){
                        instance.validateNameSyntax(["input"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the let keyword", function(){
                    expect(function(){
                        instance.validateNameSyntax(["let"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the combines keyword", function(){
                    expect(function(){
                        instance.validateNameSyntax(["combines"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the takes keyword", function(){
                    expect(function(){
                        instance.validateNameSyntax(["takes"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the returns keyword", function(){
                    expect(function(){
                        instance.validateNameSyntax(["returns"], "Test Line", allowGenerics)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
            }
            
            describe("when allowing generics", function(){
                it("allows building on generic types", function(){
                    instance.validateNameSyntax(["?", "test"], "Test Line", true)
                })
                
                it("checks if the name is a primitive literal for generic types", function(){
                    instance.validateNameSyntax(["?", "base"], "Test Line", true)
                    expect(instance.getPrimitiveTypeNameByLiteral).toHaveBeenCalledWith("base")
                })
                
                it("throws an exception given a primitive type's name following a generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "primitiveB"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedPrimitiveTypeGiven"
                    })
                })
                
                it("throws an exception given a two non-generic parameters", function(){
                    expect(function(){
                        instance.validateNameSyntax(["base", "two"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                    })
                })
                
                it("throws an exception given a two generic parameters", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "?"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "onlyOneNameOrANameFollowingAGenericParameterAllowed"
                    })
                })
                
                it("throws an exception if the name contains the input keyword following a generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "input"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the let keyword following a generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "let"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the combines keyword following a generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "combines"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the takes keyword following a generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "takes"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                it("throws an exception if the name contains the returns keyword following a generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "returns"], "Test Line", true)
                    }).toThrow({
                        line: "Test Line",
                        message: "nameExpectedKeywordGiven"
                    })
                })
                
                sharedTests(true)
            })
            
            describe("when not allowing generics", function(){
                it("throws an exception if the first token is a generic", function(){
                    expect(function(){
                        instance.validateNameSyntax(["?", "combination"], "Test Line", false)
                    }).toThrow({
                        line: "Test Line",
                        message: "genericTypeUsedInNonGenericContext"
                    })
                })
                
                sharedTests(false)
            })
        })
    })
})