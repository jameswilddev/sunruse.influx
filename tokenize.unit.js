describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("tokenize", function(){
            it("returns empty when the file is empty", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", "")).toEqual([])
            })
            
            it("returns empty when the file contains only empty lines", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", ["", "", ""].join("\n"))).toEqual([])
            })
            
            it("returns populated lines as lists of tokens", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set", 
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores whitespace at the end of lines", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set    ", 
                    "line",
                    "line five   "
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores extra whitespace between tokens", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two     token set", 
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores blank lines at the start of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "",
                    "line one tokens", 
                    "line two token set", 
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores whitespace lines at the start of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "       ",
                    "line one tokens", 
                    "line two token set", 
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores comments at the start of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "# A comment.",
                    "line one tokens", 
                    "line two token set", 
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores blank lines at the end of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set", 
                    "line",
                    "line five",
                    ""
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores whitespace lines at the end of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set", 
                    "line",
                    "line five",
                    "      "
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores comments at the end of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set", 
                    "line",
                    "line five",
                    "# A comment."
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores blank lines in the middle of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set", 
                    "",
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores whitespace lines in the middle of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set", 
                    "     ",
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    tokens: ["line", "five"]
                }])
            })
            
            it("ignores comments in the middle of the file", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "line two token set", 
                    "# A comment.",
                    "line",
                    "line five"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    tokens: ["line", "five"]
                }])
            })
            
            it("inserts an indent between lines at different indent depths", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    unindent: true
                }])
            })
            
            it("inserts another indent on indenting further", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "      line three tokens"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    unindent: true
                }, {
                    filename: "Test Filename",
                    line: 4,
                    unindent: true
                }])
            })
            
            it("supports returning to previous indentation levels", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "      line three tokens",
                    "    line four token set" 
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    unindent: true
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "four", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    unindent: true
                }])
            })
            
            it("supports going down multiple indentation levels", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "      line three tokens",
                    "        line four tokens",
                    "    line five token set" 
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "four", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    unindent: true
                }, {
                    filename: "Test Filename",
                    line: 5,
                    unindent: true
                }, {
                    filename: "Test Filename",
                    line: 5,
                    tokens: ["line", "five", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 6,
                    unindent: true
                }])
            })
            
            it("supports going down multiple indentation levels to the start", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "      line three tokens",
                    "line four token set" 
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 3,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 3,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    unindent: true
                }, {
                    filename: "Test Filename",
                    line: 4,
                    unindent: true
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "four", "token", "set"]
                }])
            })
            
            it("throws an exception should an attempt be made to return to a previously unmentioned indentation level", function(){
                expect(function(){
                    SUNRUSE.influx().tokenize("Test Filename", [
                        "       ",
                        "line one tokens", 
                        "  line two token set", 
                        "    line",
                        " line five",
                        "more data"
                    ].join("\n"))
                }).toThrow({
                    message: "unindentedToUnexpectedLevel",
                    line: 5,
                    filename: "Test Filename"
                })
            })
            
            it("does not lose the indentation level on encountering a blank line", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "",
                    "    line three tokens"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    unindent: true
                }])
            })
            
            it("does not lose the indentation level on encountering a whitespace line", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "      ",
                    "    line three tokens"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    unindent: true
                }])
            })
            
            it("does not lose the indentation level on encountering a comment", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "# A comment.",
                    "    line three tokens"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    unindent: true
                }])
            })
            
            it("ignores indented comments", function(){
                expect(SUNRUSE.influx().tokenize("Test Filename", [
                    "line one tokens", 
                    "    line two token set", 
                    "    # A comment.",
                    "    line three tokens"
                ].join("\n"))).toEqual([{
                    filename: "Test Filename",
                    line: 1,
                    tokens: ["line", "one", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 2,
                    indent: true
                }, {
                    filename: "Test Filename",
                    line: 2,
                    tokens: ["line", "two", "token", "set"]
                }, {
                    filename: "Test Filename",
                    line: 4,
                    tokens: ["line", "three", "tokens"]
                }, {
                    filename: "Test Filename",
                    line: 5,
                    unindent: true
                }])
            })
        })
    })
})