describe("SUNRUSE", function(){
    describe("influx", function(){
        describe("checkForCombinationLoops", function(){
            var instance
            beforeEach(function(){
                instance = SUNRUSE.influx()
            })
            
            it("does not throw an exception for primitive fields", function(){
                instance.checkForCombinationLoops({
                    combinations: {
                        nonGenericA: {
                            fields: {
                                fieldAA: {
                                    of: ["float"]
                                },
                                fieldAB: {
                                    of: ["int"]
                                }
                            }
                        },
                        nonGenericB: {
                            fields: {
                                fieldBA: {
                                    of: ["bool"]
                                },
                                fieldBB: {
                                    of: ["int"]
                                }
                            }
                        }
                    },
                    genericCombinations: {
                        genericC: {
                            fields: {
                                fieldCA: {
                                    of: ["float"]
                                },
                                fieldCB: {
                                    of: ["int"]
                                }
                            }
                        },
                        genericD: {
                            fields: {
                                fieldDA: {
                                    of: ["bool"]
                                },
                                fieldDB: {
                                    of: ["?"]
                                }
                            }
                        }
                    }                    
                })
            })
            
            it("does not throw an exception for other combinations as field bases", function(){
                instance.checkForCombinationLoops({
                    combinations: {
                        nonGenericA: {
                            fields: {
                                fieldAA: {
                                    of: ["nonGenericB"]
                                },
                                fieldAB: {
                                    of: ["int"]
                                }
                            }
                        },
                        nonGenericB: {
                            fields: {
                                fieldBA: {
                                    of: ["bool"]
                                },
                                fieldBB: {
                                    of: ["int"]
                                }
                            }
                        }
                    },
                    genericCombinations: {
                        genericC: {
                            fields: {
                                fieldCA: {
                                    of: ["nonGenericA"]
                                },
                                fieldCB: {
                                    of: ["int"]
                                }
                            }
                        },
                        genericD: {
                            fields: {
                                fieldDA: {
                                    of: ["bool"]
                                },
                                fieldDB: {
                                    of: ["?"]
                                }
                            }
                        }
                    }                    
                })
            })
            
            it("does not throw an exception for other combinations as field chains", function(){
                instance.checkForCombinationLoops({
                    combinations: {
                        nonGenericA: {
                            fields: {
                                fieldAA: {
                                    of: ["float"]
                                },
                                fieldAB: {
                                    of: ["int"]
                                }
                            }
                        },
                        nonGenericB: {
                            fields: {
                                fieldBA: {
                                    of: ["bool"]
                                },
                                fieldBB: {
                                    of: ["int", "genericD"]
                                }
                            }
                        }
                    },
                    genericCombinations: {
                        genericC: {
                            fields: {
                                fieldCA: {
                                    of: ["float"]
                                },
                                fieldCB: {
                                    of: ["int"]
                                }
                            }
                        },
                        genericD: {
                            fields: {
                                fieldDA: {
                                    of: ["bool", "genericC"]
                                },
                                fieldDB: {
                                    of: ["?"]
                                }
                            }
                        }
                    }                    
                })
            })
            
            it("throws an exception when a combination references itself as a base type", function(){
                expect(function(){
                    instance.checkForCombinationLoops({
                        combinations: {
                            nonGenericA: {
                                fields: {
                                    fieldAA: {
                                        of: ["float"],
                                        line: "Test Line AA"
                                    },
                                    fieldAB: {
                                        of: ["int"],
                                        line: "Test Line AB"
                                    }
                                }
                            },
                            nonGenericB: {
                                fields: {
                                    fieldBA: {
                                        of: ["bool"],
                                        line: "Test Line BA"
                                    },
                                    fieldBB: {
                                        of: ["nonGenericB"],
                                        line: "Test Line BB"
                                    }
                                }
                            }
                        },
                        genericCombinations: {
                            genericC: {
                                fields: {
                                    fieldCA: {
                                        of: ["float"],
                                        line: "Test Line CA"
                                    },
                                    fieldCB: {
                                        of: ["int"],
                                        line: "Test Line CB"
                                    }
                                }
                            },
                            genericD: {
                                fields: {
                                    fieldDA: {
                                        of: ["bool"],
                                        line: "Test Line DA"
                                    },
                                    fieldDB: {
                                        of: ["?"],
                                        line: "Test Line DB"
                                    }
                                }
                            }
                        }                    
                    })                    
                }).toThrow({
                    message: "infiniteLoopInCombination",
                    line: "Test Line BB"
                })
            })
            
            it("throws an exception when a combination references itself as a chain type", function(){
                expect(function(){
                    instance.checkForCombinationLoops({
                        combinations: {
                            nonGenericA: {
                                fields: {
                                    fieldAA: {
                                        of: ["float"],
                                        line: "Test Line AA"
                                    },
                                    fieldAB: {
                                        of: ["int"],
                                        line: "Test Line AB"
                                    }
                                }
                            },
                            nonGenericB: {
                                fields: {
                                    fieldBA: {
                                        of: ["bool"],
                                        line: "Test Line BA"
                                    },
                                    fieldBB: {
                                        of: ["int"],
                                        line: "Test Line BB"
                                    }
                                }
                            }
                        },
                        genericCombinations: {
                            genericC: {
                                fields: {
                                    fieldCA: {
                                        of: ["float"],
                                        line: "Test Line CA"
                                    },
                                    fieldCB: {
                                        of: ["int"],
                                        line: "Test Line CB"
                                    }
                                }
                            },
                            genericD: {
                                fields: {
                                    fieldDA: {
                                        of: ["bool", "genericD"],
                                        line: "Test Line DA"
                                    },
                                    fieldDB: {
                                        of: ["?"],
                                        line: "Test Line DB"
                                    }
                                }
                            }
                        }                    
                    })                    
                }).toThrow({
                    message: "infiniteLoopInCombination",
                    line: "Test Line DA"
                })
            })            
            
            it("throws an exception when two combinations reference one another as base types", function(){
                expect(function(){
                    instance.checkForCombinationLoops({
                        combinations: {
                            nonGenericA: {
                                fields: {
                                    fieldAA: {
                                        of: ["nonGenericB"],
                                        line: "Test Line Expected"
                                    },
                                    fieldAB: {
                                        of: ["int"],
                                        line: "Test Line AB"
                                    }
                                }
                            },
                            nonGenericB: {
                                fields: {
                                    fieldBA: {
                                        of: ["bool"],
                                        line: "Test Line BA"
                                    },
                                    fieldBB: {
                                        of: ["nonGenericA"],
                                        line: "Test Line Expected"
                                    }
                                }
                            }
                        },
                        genericCombinations: {
                            genericC: {
                                fields: {
                                    fieldCA: {
                                        of: ["float"],
                                        line: "Test Line CA"
                                    },
                                    fieldCB: {
                                        of: ["int"],
                                        line: "Test Line CB"
                                    }
                                }
                            },
                            genericD: {
                                fields: {
                                    fieldDA: {
                                        of: ["bool"],
                                        line: "Test Line DA"
                                    },
                                    fieldDB: {
                                        of: ["?"],
                                        line: "Test Line DB"
                                    }
                                }
                            }
                        }                    
                    })                    
                }).toThrow({
                    message: "infiniteLoopInCombination",
                    line: "Test Line Expected"
                })
            })            
            
            it("throws an exception when two combinations reference one another as chain types", function(){
                expect(function(){
                    instance.checkForCombinationLoops({
                        combinations: {
                            nonGenericA: {
                                fields: {
                                    fieldAA: {
                                        of: ["float"],
                                        line: "Test Line AA"
                                    },
                                    fieldAB: {
                                        of: ["int"],
                                        line: "Test Line AB"
                                    }
                                }
                            },
                            nonGenericB: {
                                fields: {
                                    fieldBA: {
                                        of: ["bool"],
                                        line: "Test Line BA"
                                    },
                                    fieldBB: {
                                        of: ["int"],
                                        line: "Test Line BB"
                                    }
                                }
                            }
                        },
                        genericCombinations: {
                            genericC: {
                                fields: {
                                    fieldCA: {
                                        of: ["float"],
                                        line: "Test Line CA"
                                    },
                                    fieldCB: {
                                        of: ["?", "genericD"],
                                        line: "Test Line Expected"
                                    }
                                }
                            },
                            genericD: {
                                fields: {
                                    fieldDA: {
                                        of: ["bool", "genericC"],
                                        line: "Test Line Expected"
                                    },
                                    fieldDB: {
                                        of: ["?"],
                                        line: "Test Line DB"
                                    }
                                }
                            }
                        }                    
                    })                    
                }).toThrow({
                    message: "infiniteLoopInCombination",
                    line: "Test Line Expected"
                })
            })                        
            
            it("throws an exception when two combinations reference one another, one as a chain type, the other as a base type", function(){
                expect(function(){
                    instance.checkForCombinationLoops({
                        combinations: {
                            nonGenericA: {
                                fields: {
                                    fieldAA: {
                                        of: ["float", "genericC"],
                                        line: "Test Line Expected"
                                    },
                                    fieldAB: {
                                        of: ["int"],
                                        line: "Test Line AB"
                                    }
                                }
                            },
                            nonGenericB: {
                                fields: {
                                    fieldBA: {
                                        of: ["bool"],
                                        line: "Test Line BA"
                                    },
                                    fieldBB: {
                                        of: ["int"],
                                        line: "Test Line BB"
                                    }
                                }
                            }
                        },
                        genericCombinations: {
                            genericC: {
                                fields: {
                                    fieldCA: {
                                        of: ["float"],
                                        line: "Test Line CA"
                                    },
                                    fieldCB: {
                                        of: ["nonGenericA"],
                                        line: "Test Line Expected"
                                    }
                                }
                            },
                            genericD: {
                                fields: {
                                    fieldDA: {
                                        of: ["bool"],
                                        line: "Test Line DA"
                                    },
                                    fieldDB: {
                                        of: ["?"],
                                        line: "Test Line DB"
                                    }
                                }
                            }
                        }                    
                    })                    
                }).toThrow({
                    message: "infiniteLoopInCombination",
                    line: "Test Line Expected"
                })
            })                        
            
            it("throws an exception when three combinations reference one another in a cycle", function(){
                expect(function(){
                    instance.checkForCombinationLoops({
                        combinations: {
                            nonGenericA: {
                                fields: {
                                    fieldAA: {
                                        of: ["float", "genericC"],
                                        line: "Test Line Expected"
                                    },
                                    fieldAB: {
                                        of: ["int"],
                                        line: "Test Line AB"
                                    }
                                }
                            },
                            nonGenericB: {
                                fields: {
                                    fieldBA: {
                                        of: ["bool"],
                                        line: "Test Line BA"
                                    },
                                    fieldBB: {
                                        of: ["nonGenericA"],
                                        line: "Test Line Expected"
                                    }
                                }
                            }
                        },
                        genericCombinations: {
                            genericC: {
                                fields: {
                                    fieldCA: {
                                        of: ["float"],
                                        line: "Test Line CA"
                                    },
                                    fieldCB: {
                                        of: ["nonGenericB"],
                                        line: "Test Line Expected"
                                    }
                                }
                            },
                            genericD: {
                                fields: {
                                    fieldDA: {
                                        of: ["bool"],
                                        line: "Test Line DA"
                                    },
                                    fieldDB: {
                                        of: ["?"],
                                        line: "Test Line DB"
                                    }
                                }
                            }
                        }                    
                    })                    
                }).toThrow({
                    message: "infiniteLoopInCombination",
                    line: "Test Line Expected"
                })
            })                        
        })
    })
})